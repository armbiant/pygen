import socket

from typing import List

from pygenshell.client.abc import ShellClient
from pygenshell.std.stream.abc import ShellStdStream
from pygentype.address.abc import Address
from pygentype.err import ExpectedException
from pygentype.err import UnreachableStateException


class ShellTcpClientImpl(ShellClient):
    _address: Address

    def __init__(self, address: Address) -> None:
        self._address = address

    def call(
        self,
        args: List[str],
        stdout: ShellStdStream,
        stderr: ShellStdStream
    ) -> None:
        so = "so"  # stdout
        se = "se"  # stderr
        rc = "rc"  # return-code
        cmd_tpl = ";".join([
            "exec 3>&1",
            " ".join([
                "({args})",
                " 2> >(while read line; do echo \"{se}:$line\" >&3; done)",
                " 1> >(while read line; do echo \"{so}:$line\" >&3; done)"
            ]),
            "rc=$?",
            "sync",
            "echo \"{rc}:${{rc}}\" >&3",
            "\n"
        ])
        cmd = cmd_tpl.format(
            args=" ".join(args),
            se=se,
            so=so,
            rc=rc
        )

        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        with (client):
            try:
                client.connect(tuple(self._address))
            except ConnectionRefusedError as e:
                raise ExpectedException(e.strerror)

            client.sendall(cmd.encode())
            while (True):
                marker = self._receive(client, ":")
                content = self._receive(client, "\n")

                if (marker == so):
                    stdout.write(content)
                    continue
                elif (marker == se):
                    stderr.write(content)
                    continue
                elif (marker == rc):
                    pass
                else:
                    raise UnreachableStateException()

                if (content != "0"):
                    raise ExpectedException(
                        msg="Return code [{}]".format(
                            content
                        )
                    )
                else:
                    return

    def _receive(self, client: socket.socket, delimiter: str) -> str:
        result = client.recv(1).decode()
        while (result[-1] != delimiter):
            result += client.recv(1).decode()

        return result[:-1]
