class ShellStdStream:
    def write(self, text: str) -> None:
        raise NotImplementedError()
