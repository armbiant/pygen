from typing import Callable
from typing import Protocol

from pygenshell.std.stream.abc import ShellStdStream


class ShellUnclassifiedLogger(Protocol):
    def unclassified(self, msg: str) -> None:
        raise NotImplementedError()


class ShellStdStreamAdapterImpl(ShellStdStream):
    @staticmethod
    def create_unclassified_logger(
        logger: ShellUnclassifiedLogger
    ) -> ShellStdStream:
        def _transform(text: str) -> None:
            logger.unclassified(text)

        return ShellStdStreamAdapterImpl(_transform)

    _transform_fn: Callable[[str], None]

    def __init__(self, transform_fn: Callable[[str], None]) -> None:
        self._transform_fn = transform_fn

    def write(self, text: str) -> None:
        self._transform_fn(text)
