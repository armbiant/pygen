from pygenshell.std.stream.abc import ShellStdStream


class ShellStdStreamBufferImpl(ShellStdStream):
    _text: str

    def __init__(self) -> None:
        self._text = ""

    def write(self, text: str) -> None:
        self._text += text

    def text(self) -> str:
        return self._text
