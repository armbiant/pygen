from typing import List

from pygenshell.std.stream.abc import ShellStdStream


class ShellClient:
    def call(
        self,
        args: List[str],
        stdout: ShellStdStream,
        stderr: ShellStdStream
    ) -> None:
        raise NotImplementedError()
