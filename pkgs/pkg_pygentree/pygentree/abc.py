from __future__ import annotations

from typing import Sequence


class TreeObjectNodeContext:
    def detach_text_node(
        self,
        name: str
    ) -> str:
        raise NotImplementedError()

    def detach_bool_node(
        self,
        name: str
    ) -> bool:
        raise NotImplementedError()

    def detach_float_node(
        self,
        name: str
    ) -> float:
        raise NotImplementedError()

    def detach_int_node(
        self,
        name: str
    ) -> int:
        raise NotImplementedError()

    def detach_object_node(
        self,
        name: str
    ) -> TreeObjectNode:
        raise NotImplementedError()

    def detach_list_node(
        self,
        name: str
    ) -> TreeListNode:
        raise NotImplementedError()


class TreeObjectNode:
    def path(self) -> str:
        raise NotImplementedError()

    def names(self) -> Sequence[str]:
        raise NotImplementedError()

    def __enter__(self) -> TreeObjectNodeContext:
        raise NotImplementedError()

    def __exit__(
        self,
        type: object,
        value: object,
        traceback: object
    ) -> None:
        raise NotImplementedError()


class TreeListNodeContext:
    def empty(self) -> bool:
        raise NotImplementedError()

    def detach_object_node(self) -> TreeObjectNode:
        raise NotImplementedError()


class TreeListNode:
    def path(self) -> str:
        raise NotImplementedError()

    def __enter__(self) -> TreeListNodeContext:
        raise NotImplementedError()

    def __exit__(
        self,
        type: object,
        value: object,
        traceback: object
    ) -> None:
        raise NotImplementedError()
