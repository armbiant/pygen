from typing import Dict
from typing import Mapping
from typing import NoReturn
from typing import Sequence
from typing import Type
from typing import cast

from pygentree.abc import TreeListNode
from pygentree.abc import TreeObjectNode
from pygentree.abc import TreeObjectNodeContext
from pygentree.err import TreeInvalidNodeTypeException
from pygentree.err import TreeNodeNotFoundException
from pygentree.err import TreeUnexpectedNodesException


class TreeObjectNodeImpl(TreeObjectNodeContext, TreeObjectNode):
    @staticmethod
    def create(tree: object, path: str = "$") -> TreeObjectNode:
        if (isinstance(tree, dict)):
            tree = cast(Mapping[object, object], tree)

            for name in tree.keys():
                if (not isinstance(name, str)):
                    msg_tpl = (
                        "Invalid node name type. [{}] instead of str"
                    )

                    raise TreeInvalidNodeTypeException(
                        msg=msg_tpl.format(
                            type(name).__name__
                        )
                    )

            tree = cast(Mapping[str, object], tree)

            return TreeObjectNodeImpl(
                path=path,
                tree=tree
            )
        else:
            raise TreeInvalidNodeTypeException.create(
                path=path,
                expected_type="dict",
                actual_type=type(tree).__name__
            )

    _path: str
    _nodes: Dict[str, object]

    def __init__(
        self,
        path: str,
        tree: Mapping[str, object]
    ) -> None:
        self._path = path
        self._nodes = dict(tree)

    def path(self) -> str:
        return self._path

    def names(self) -> Sequence[str]:
        return [*self._nodes.keys()]

    def detach_text_node(
        self,
        name: str
    ) -> str:
        t = str

        node = self._detach_node(name)

        if (isinstance(node, t)):
            return node

        self._raise_exception_invalid_type(name, node, t)

    def detach_bool_node(
        self,
        name: str
    ) -> bool:
        t = bool

        node = self._detach_node(name)

        if (isinstance(node, t) and (type(node) == t)):
            return node

        self._raise_exception_invalid_type(name, node, t)

    def detach_float_node(
        self,
        name: str
    ) -> float:
        t = float

        node = self._detach_node(name)

        if (isinstance(node, t) and (type(node) == t)):
            return node

        self._raise_exception_invalid_type(name, node, t)

    def detach_int_node(
        self,
        name: str
    ) -> int:
        t = int

        node = self._detach_node(name)

        if (isinstance(node, t) and (type(node) == t)):
            return node

        self._raise_exception_invalid_type(name, node, t)

    def detach_object_node(
        self,
        name: str
    ) -> TreeObjectNode:
        node = self._detach_node(name)

        try:
            return TreeObjectNodeImpl.create(node, self._node_path(name))
        except TreeInvalidNodeTypeException:
            self._raise_exception_invalid_type(name, node, dict)

    def detach_list_node(
        self,
        name: str
    ) -> TreeListNode:
        from pygentree.listnode.impl import TreeListNodeImpl

        node = self._detach_node(name)

        try:
            return TreeListNodeImpl.create(node, self._node_path(name))
        except TreeInvalidNodeTypeException:
            self._raise_exception_invalid_type(name, node, list)

    def __enter__(self) -> TreeObjectNodeContext:
        return self

    def __exit__(
        self,
        type: object,
        value: object,
        traceback: object
    ) -> None:
        if (value is not None):
            return

        if (len(self._nodes) > 0):
            raise TreeUnexpectedNodesException.create(
                nodes="{}{}".format(
                    self.path(),
                    sorted(self.names())
                )
            )

    def _raise_exception_invalid_type(
        self,
        name: str,
        node: object,
        t: Type[object]
    ) -> NoReturn:
        raise TreeInvalidNodeTypeException.create(
            path=self._node_path(name),
            expected_type=t.__name__,
            actual_type=type(node).__name__
        )

    def _node_path(self, name: str) -> str:
        return "{}.{}".format(self._path, name)

    def _detach_node(
        self,
        name: str
    ) -> object:
        node = self._nodes.pop(name, None)

        if (node is not None):
            return node
        else:
            raise TreeNodeNotFoundException.create(self._node_path(name))

    def __eq__(self, __o: object) -> bool:
        if (not isinstance(__o, TreeObjectNodeImpl)):
            return False

        if (self._path != __o._path):
            return False

        if (self._nodes != __o._nodes):
            return False

        return True
