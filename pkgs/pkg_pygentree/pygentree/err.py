from pygentype.err import UserException


class TreeException(UserException):
    ...


class TreeInvalidNodeTypeException(TreeException):
    @staticmethod
    def create(
        path: str,
        expected_type: str,
        actual_type: str
    ) -> UserException:
        return TreeInvalidNodeTypeException(
            msg="Node [{}]: Invalid node type. [{}] instead of [{}]".format(
                path,
                actual_type,
                expected_type
            )
        )


class TreeNodeNotFoundException(TreeException):
    @staticmethod
    def create(path: str) -> UserException:
        return TreeNodeNotFoundException(
            msg="Node [{}] not found".format(
                path
            )
        )


class TreeUnexpectedNodesException(TreeException):
    @staticmethod
    def create(nodes: str) -> UserException:
        return TreeUnexpectedNodesException(
            msg="Unexpected node(s) [{}]".format(
                nodes
            )
        )
