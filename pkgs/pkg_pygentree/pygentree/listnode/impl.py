from typing import NoReturn
from typing import Sequence
from typing import Type
from typing import cast

from pygentree.abc import TreeListNode
from pygentree.abc import TreeListNodeContext
from pygentree.abc import TreeObjectNode
from pygentree.err import TreeInvalidNodeTypeException
from pygentree.err import TreeUnexpectedNodesException


class TreeListNodeImpl(TreeListNodeContext, TreeListNode):
    @staticmethod
    def create(tree: object, path: str = "$") -> TreeListNode:
        if (isinstance(tree, list)):
            tree = cast(Sequence[object], tree)

            return TreeListNodeImpl(
                path=path,
                tree=tree
            )
        else:
            raise TreeInvalidNodeTypeException.create(
                path=path,
                expected_type="dict",
                actual_type=type(tree).__name__
            )

    _path: str
    _nodes: Sequence[object]
    _index: int

    def __init__(
        self,
        path: str,
        tree: Sequence[object]
    ) -> None:
        self._path = path
        self._nodes = list(tree)
        self._index = -1

    def path(self) -> str:
        return self._path

    def empty(self) -> bool:
        return (self._index >= (len(self._nodes) - 1))

    def detach_object_node(self) -> TreeObjectNode:
        from pygentree.objectnode.impl import TreeObjectNodeImpl

        node = self._detach_node()

        try:
            return TreeObjectNodeImpl.create(node, self._node_path())
        except TreeInvalidNodeTypeException:
            self._raise_exception_invalid_type(node, dict)

    def __enter__(self) -> TreeListNodeContext:
        return self

    def __exit__(
        self,
        type: object,
        value: object,
        traceback: object
    ) -> None:
        if (value is not None):
            return

        if (not self.empty()):
            raise TreeUnexpectedNodesException.create(
               nodes="{}[{}:{}]".format(
                    self.path(),
                    self._index + 1,
                    len(self._nodes) - 1
                )
            )

    def _raise_exception_invalid_type(
        self,
        node: object,
        t: Type[object]
    ) -> NoReturn:
        raise TreeInvalidNodeTypeException.create(
            path=self._node_path(),
            expected_type=t.__name__,
            actual_type=type(node).__name__
        )

    def _detach_node(self) -> object:
        self._index += 1

        return self._nodes[self._index]

    def _node_path(self) -> str:
        return "{}[{}]".format(self._path, self._index)

    def __eq__(self, __o: object) -> bool:
        if (not isinstance(__o, TreeListNodeImpl)):
            return False

        if (self._path != __o._path):
            return False

        if (self._nodes != __o._nodes):
            return False

        return True
