from __future__ import annotations

from typing import Callable

from pygencli.arg.abc import CliArg


class CliArgMock(CliArg):
    @staticmethod
    def create(text_generator_fn: Callable[[], str]) -> CliArg:
        return CliArgMock(
            metavar=text_generator_fn(),
            name=text_generator_fn()
        )

    _metavar: str
    _name: str

    def __init__(
        self,
        metavar: str,
        name: str
    ) -> None:
        self._metavar = metavar
        self._name = name

    def metavar(self) -> str:
        return self._metavar

    def name(self) -> str:
        return self._name
