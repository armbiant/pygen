from __future__ import annotations

from typing import Callable

from pygencli.arg.abc import CliArg
from pygencli.option.abc import CliOption
from pygenclitest.arg.mock import CliArgMock


class CliOptionMock(CliOption):
    @staticmethod
    def create(text_generator_fn: Callable[[], str]) -> CliOption:
        return CliOptionMock(
            name=text_generator_fn(),
            arg=CliArgMock.create(text_generator_fn)
        )

    _name: str
    _arg: CliArg

    def __init__(
        self,
        name: str,
        arg: CliArg
    ) -> None:
        self._name = name
        self._arg = arg

    def name(self) -> str:
        return self._name

    def arg(self) -> CliArg:
        return self._arg
