from __future__ import annotations

from typing import ContextManager
from typing import List
from typing import Mapping
from typing import NamedTuple
from typing import Protocol
from typing import Sequence

from pygentest.testcase.abc import TestCase
from pygentype.err import UnreachableStateException


class ExceptionMock(NamedTuple):
    params: Mapping[str, object]
    msg: str


class TestCaseMock(TestCase):
    class TestCaseContext(Protocol):
        def params(self) -> Mapping[str, object]:
            raise UnreachableStateException()

        def ref(self) -> TestCaseMock.TestCaseContext:
            raise UnreachableStateException()

    class TestCaseContextImpl(TestCaseContext):
        def params(self) -> Mapping[str, object]:
            return {}

        def ref(self) -> TestCaseMock.TestCaseContext:
            raise UnreachableStateException()

    class SubTestCaseContextImpl(TestCaseContext):
        _params: Mapping[str, object]
        _ref: TestCaseMock.TestCaseContext

        def __init__(
            self,
            params: Mapping[str, object],
            ref: TestCaseMock.TestCaseContext
        ) -> None:
            self._params = params
            self._ref = ref

        def params(self) -> Mapping[str, object]:
            return self._params

        def ref(self) -> TestCaseMock.TestCaseContext:
            return self._ref

    _context: TestCaseMock.TestCaseContext
    _exceptions: List[ExceptionMock]

    def __init__(self) -> None:
        self._context = TestCaseMock.TestCaseContextImpl()
        self._exceptions = []

    def exception(
        self,
        params: Mapping[str, object],
        msg: str
    ) -> ExceptionMock:
        return ExceptionMock(params, msg)

    def exceptions(self) -> Sequence[ExceptionMock]:
        return self._exceptions

    def assertTrue(
        self,
        expr: bool,
        msg: str = ""
    ) -> None:
        if (not expr):
            self._exceptions.append(
                self.exception(self._context.params(), "")
            )

    def assertFalse(
        self,
        expr: bool,
        msg: str = ""
    ) -> None:
        self.assertTrue(
            expr=(not expr),
            msg=msg
        )

    def assertEqual(
        self,
        first: object,
        second: object,
        msg: str = ""
    ) -> None:
        if (first != second):
            self._exceptions.append(
                self.exception(
                    params=self._context.params(),
                    msg=msg
                )
            )

    def subTest(
        self,
        msg: object = ...,
        **params: object
    ) -> ContextManager[None]:
        self._context = TestCaseMock.SubTestCaseContextImpl(
            params=params,
            ref=self._context
        )

        return self

    def __enter__(self) -> None:
        return None

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        self._context = self._context.ref()
