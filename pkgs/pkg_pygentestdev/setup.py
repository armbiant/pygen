#!/usr/bin/env python

import os

from typing import Sequence

from setuptools import find_packages
from setuptools import setup


def read_requirements() -> Sequence[str]:
    with (open("requirements.txt", "r") as f):
        return f.read().splitlines()


setup(
    name="pygentestdev",
    version=os.environ.get("BUILD_ARG_VERSION", "0.0.dev"),
    author="Vladyslav Kazakov",
    author_email="kazakov1048576@gmail.com",
    url="https://gitlab.com/1048576/lib.d/pygen",
    install_requires=read_requirements(),
    package_data={
        "pygentestdev": ["py.typed"]
    },
    packages=find_packages(
        include=["pygentestdev", "pygentestdev.*"]
    ),
    license="MIT"
)
