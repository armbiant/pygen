from socket import socket
from threading import Thread

from pygentype.address.abc import Address


class ShellTcpServerMock(Thread):
    _server_socket: socket
    _response: str

    def __init__(self, response: str) -> None:
        super().__init__()
        self._server_socket = socket()
        self._response = response

    def run(self) -> None:
        connection, _ = self._server_socket.accept()
        with (connection):
            connection.sendall(self._response.encode())

    def address(self) -> Address:
        return self._server_socket.getsockname()

    def __enter__(self) -> None:
        self._server_socket.bind(("127.0.0.1", 0))
        self._server_socket.listen(1)
        self.start()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        self._server_socket.close()
