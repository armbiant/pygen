from pygenarg.address.abc import AddressArg
from pygenarg.address.abc import AddressArgContext
from pygenlog.logger.abc import Logger
from pygentype.address.abc import Address
from pygentype.err import UserException


class AddressArgImpl(AddressArg, AddressArgContext):
    _description: str
    _value: Address

    def __init__(
        self,
        description: str,
        value: Address
    ) -> None:
        self._description = description
        self._value = value

    def value(self) -> Address:
        return self._value

    def describe(self, logger: Logger) -> None:
        logger.info(
            msg="{}: [{}]".format(
                self._description,
                self._value
            )
        )

    def __enter__(self) -> AddressArgContext:
        return self

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        if (isinstance(value, UserException)):
            raise value.wrap(
                title="{}: [{}]".format(
                    self._description,
                    self._value
                )
            )
