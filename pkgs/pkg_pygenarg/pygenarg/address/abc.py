from pygenarg.abc import Arg
from pygentype.address.abc import Address


class AddressArgContext:
    def value(self) -> Address:
        raise NotImplementedError()


class AddressArg(Arg[AddressArgContext]):
    pass
