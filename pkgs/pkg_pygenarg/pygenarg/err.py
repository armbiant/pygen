from pygentype.err import UserException


class ArgEmptyValueException(UserException):
    @staticmethod
    def create(description: str) -> UserException:
        return ArgEmptyValueException(
            msg="[{}]: empty value".format(description)
        )
