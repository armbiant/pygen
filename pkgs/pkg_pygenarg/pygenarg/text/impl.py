from typing import Mapping

from pygenarg.err import ArgEmptyValueException
from pygenarg.text.abc import TextArg
from pygenarg.text.abc import TextArgContext
from pygenlog.logger.abc import Logger
from pygentype.err import UserException


class TextArgImpl(TextArg, TextArgContext):
    @staticmethod
    def create_required(description: str, value: str) -> TextArg:
        if (value == ""):
            raise ArgEmptyValueException.create(
                description=description
            )

        return TextArgImpl(description, value)

    @staticmethod
    def create_optional(description: str, value: str) -> TextArg:
        return TextArgImpl.create_required(description, value)

    @staticmethod
    def create_required_env(envs: Mapping[str, str], env_name: str) -> TextArg:
        return TextArgImpl.create_required(
            description="Env[{}]".format(env_name),
            value=envs.get(env_name, "")
        )

    @staticmethod
    def create_optional_env(envs: Mapping[str, str], env_name: str) -> TextArg:
        return TextArgImpl.create_required_env(envs, env_name)

    _description: str
    _value: str

    def __init__(
        self,
        description: str,
        value: str
    ) -> None:
        self._description = description
        self._value = value

    def value(self) -> str:
        return self._value

    def describe(self, logger: Logger) -> None:
        logger.info(
            msg="{}: [{}]".format(
                self._description,
                self._value
            )
        )

    def __enter__(self) -> TextArgContext:
        return self

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        if (isinstance(value, UserException)):
            raise value.wrap(
                title="{}: [{}]".format(
                    self._description,
                    self._value
                )
            )
