from pygenarg.abc import Arg


class TextArgContext:
    def value(self) -> str:
        raise NotImplementedError()


class TextArg(Arg[TextArgContext]):
    pass
