from pygenarg.abc import Arg
from pygenpath.abc import PathResolver


class PathResolverArgContext:
    def value(self) -> PathResolver:
        raise NotImplementedError()


class PathResolverArg(Arg[PathResolverArgContext]):
    pass
