from pygenarg.path.abc import PathResolverArg
from pygenarg.path.abc import PathResolverArgContext
from pygenlog.logger.abc import Logger
from pygenpath.abc import PathResolver
from pygentype.err import UserException


class PathResolverArgImpl(PathResolverArg, PathResolverArgContext):
    _description: str
    _value: PathResolver

    def __init__(
        self,
        description: str,
        value: PathResolver
    ) -> None:
        self._description = description
        self._value = value

    def value(self) -> PathResolver:
        return self._value

    def describe(self, logger: Logger) -> None:
        logger.info(
            msg="{}: [{}]".format(
                self._description,
                self._value.pwd()
            )
        )

    def __enter__(self) -> PathResolverArgContext:
        return self

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        if (isinstance(value, UserException)):
            raise value.wrap(
                title="{}: [{}]".format(
                    self._description,
                    self._value.pwd()
                )
            )
