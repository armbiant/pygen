from typing import Generic
from typing import TypeVar

from pygenlog.logger.abc import Logger

ArgContext = TypeVar(
    name="ArgContext",
    covariant=True
)


class Arg(Generic[ArgContext]):
    def describe(self, logger: Logger) -> None:
        raise NotImplementedError()

    def __enter__(self) -> ArgContext:
        raise NotImplementedError()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        raise NotImplementedError()
