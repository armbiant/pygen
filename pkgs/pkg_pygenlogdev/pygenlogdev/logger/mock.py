from pygenlog.logger.abc import Logger
from pygenlogdev.mock import LogEntryMock
from pygenlogdev.mock import LogMock


class LoggerMock(Logger):
    _log: LogMock

    def __init__(self, log: LogMock) -> None:
        self._log = log

    def info(self, msg: str) -> None:
        entry = LogEntryMock.info(msg)
        self._log.append(entry)

    def unclassified(self, msg: str) -> None:
        entry = LogEntryMock.unclassified(msg)
        self._log.append(entry)
