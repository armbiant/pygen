from __future__ import annotations

from typing import List
from typing import Tuple

from pygenlog.abc import LogEntrySeverity


class LogEntryMock(Tuple[LogEntrySeverity, str]):
    @staticmethod
    def info(msg: str) -> LogEntryMock:
        entry = (LogEntrySeverity.INFO, msg)

        return LogEntryMock(entry)

    @staticmethod
    def unclassified(msg: str) -> LogEntryMock:
        entry = (LogEntrySeverity.UNCLASSIFIED, msg)

        return LogEntryMock(entry)


LogMock = List[LogEntryMock]
