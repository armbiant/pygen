from enum import Enum


class LogEntrySeverity(Enum):
    INFO = "info"
    UNCLASSIFIED = "unclassified"
