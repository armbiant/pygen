class Logger:
    def info(self, msg: str) -> None:
        raise NotImplementedError()

    def unclassified(self, msg: str) -> None:
        raise NotImplementedError()
