import sys

from pygenlog.logger.abc import Logger


class LoggerImpl(Logger):
    def info(self, msg: str) -> None:
        print(
            "\033[36m{}\033[39m".format(msg)
        )
        sys.stdout.flush()

    def unclassified(self, msg: str) -> None:
        print(msg)
        sys.stdout.flush()
