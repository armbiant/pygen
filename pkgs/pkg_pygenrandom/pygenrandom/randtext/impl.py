import pygenrandom.utils

from pygenrandom.abc import UniqueRandomGenerator
from pygenrandom.impl import UniqueRandomGeneratorImpl
from pygenrandom.randtext.abc import UniqueRandomTextGenerator


class UniqueRandomTextGeneratorImpl(UniqueRandomTextGenerator):
    _generator: UniqueRandomGenerator[str]

    def __init__(self, retry_limit: int = 10) -> None:
        self._generator = UniqueRandomGeneratorImpl(
            retry_limit=retry_limit,
            equal_fn=lambda a, b: a == b
        )

    def randtext(
        self,
        alphabet: str = pygenrandom.utils.DEFAULT_TEXT_ALPHABET,
        size: int = pygenrandom.utils.DEFAULT_TEXT_SIZE
    ) -> str:
        return self._generator.randval(
            randval_fn=lambda: pygenrandom.utils.randtext(alphabet, size)
        )
