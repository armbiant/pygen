class UniqueRandomTextGenerator:
    def randtext(self, alphabet: str = ..., size: int = ...) -> str:
        raise NotImplementedError()
