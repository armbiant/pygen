from typing import Callable
from typing import List
from typing import TypeVar

from pygenrandom.abc import UniqueRandomGenerator
from pygentype.err import UnreachableStateException

T = TypeVar("T")


class UniqueRandomGeneratorImpl(UniqueRandomGenerator[T]):
    _values: List[T]
    _retry_limit: int
    _equal_fn: Callable[[T, T], bool]

    def __init__(
        self,
        retry_limit: int,
        equal_fn: Callable[[T, T], bool]
    ) -> None:
        self._values = []
        self._retry_limit = retry_limit
        self._equal_fn = equal_fn

    def randval(
        self,
        randval_fn: Callable[[], T]
    ) -> T:
        for _ in range(0, self._retry_limit):
            new_value = randval_fn()
            is_new_value_exists = any(
                self._equal_fn(new_value, value) for value in self._values
            )
            if (not is_new_value_exists):
                self._values.append(new_value)

                return new_value

        raise UnreachableStateException()
