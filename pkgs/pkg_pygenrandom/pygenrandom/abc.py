from typing import Callable
from typing import Generic
from typing import TypeVar

T = TypeVar("T")


class UniqueRandomGenerator(Generic[T]):
    def randval(
        self,
        randval_fn: Callable[[], T]
    ) -> T:
        raise NotImplementedError()
