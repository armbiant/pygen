import random
import string

DEFAULT_TEXT_ALPHABET: str = string.ascii_lowercase
DEFAULT_TEXT_SIZE: int = 10


def randtext(
    alphabet: str = DEFAULT_TEXT_ALPHABET,
    size: int = DEFAULT_TEXT_SIZE
) -> str:
    text = ""

    for _ in range(0, size):
        text += random.choice(alphabet)

    return text
