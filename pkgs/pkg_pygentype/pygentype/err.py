class UnreachableStateException(Exception):
    def __init__(self) -> None:
        super().__init__("Unreachable state")


class UserException(Exception):
    def __init__(self, msg: str) -> None:
        super().__init__(msg)

    def wrap(self, title: str) -> Exception:
        raise NotImplementedError()

    def __eq__(self, other: object) -> bool:
        if (self.__class__ != other.__class__):
            return False

        return (str(self) == str(other))


class CommonUserException(UserException):
    def wrap(self, title: str) -> Exception:
        e = self.__new__(self.__class__)

        e.__init__(
            msg="{}:\n  {}".format(
                title,
                str(self).replace("\n", "\n  ")
            )
        )

        return e


class OSUserException(CommonUserException):
    @staticmethod
    def create(e: OSError) -> Exception:
        if (isinstance(e, FileNotFoundError)):
            return OSUserException.file_not_found()
        else:
            return e

    @staticmethod
    def file_not_found() -> Exception:
        return OSUserException("File not found")


ExpectedException = UserException
CommonExpectedException = CommonUserException
OSExpectedException = OSUserException
