from __future__ import annotations

from typing import Generic
from typing import Iterable
from typing import Protocol
from typing import TypeVar

T = TypeVar(
    name="T"
)


class ReadonlySet(Generic[T], Iterable[T], Protocol):
    def __contains__(self, __o: object) -> bool:
        raise NotImplementedError()

    def difference(self, *s: Iterable[T]) -> ReadonlySet[T]:
        raise NotImplementedError()

    def intersection(self, *s: Iterable[T]) -> ReadonlySet[T]:
        raise NotImplementedError()
