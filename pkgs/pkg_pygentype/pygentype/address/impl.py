from pygentype.address.abc import Address
from pygentype.err import UserException


class AddressImpl(Address):
    @staticmethod
    def create(address: str) -> Address:
        try:
            host, port = tuple(address.split(":"))
        except ValueError:
            raise UserException("Invalid address")

        try:
            return AddressImpl(host, int(port))
        except ValueError:
            raise UserException("Invalid port")

    def __str__(self) -> str:
        return "{}:{}".format(self.host, self.port)
