from typing import Callable
from typing import List
from typing import Sequence
from typing import Tuple
from typing import TypeVar

T = TypeVar("T")


def split_into_bucket(
    items: Sequence[T],
    fn: Callable[[T], bool]
) -> Tuple[List[T], List[T]]:
    a: List[T] = []
    b: List[T] = []

    for item in items:
        if (fn(item)):
            a.append(item)
        else:
            b.append(item)

    return (a, b)
