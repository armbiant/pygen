from netaddr.ip.sets import IPSet as NetaddrIPSet
from pygennetaddr.abc import IPSet
from pygentype.err import UnreachableStateException


class IPSetImpl(IPSet):
    @staticmethod
    def create(*addresses: str) -> IPSet:
        return IPSetImpl(
            ip_set=NetaddrIPSet(addresses)
        )

    _ip_set: NetaddrIPSet

    def __init__(self, ip_set: NetaddrIPSet) -> None:
        self._ip_set = ip_set

    def union(self, other: IPSet) -> IPSet:
        if (not isinstance(other, IPSetImpl)):
            raise UnreachableStateException()

        return IPSetImpl(
            ip_set=self._ip_set.union(other._ip_set)
        )

    def intersection(self, other: IPSet) -> IPSet:
        if (not isinstance(other, IPSetImpl)):
            raise UnreachableStateException()

        return IPSetImpl(
            ip_set=self._ip_set.intersection(other._ip_set)
        )

    def __str__(self) -> str:
        return str(self._ip_set)
