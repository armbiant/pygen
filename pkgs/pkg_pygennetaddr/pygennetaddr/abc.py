from __future__ import annotations

from typing import Protocol


class IPSet(Protocol):
    def union(self, other: IPSet) -> IPSet:
        raise NotImplementedError()

    def intersection(self, other: IPSet) -> IPSet:
        raise NotImplementedError()
