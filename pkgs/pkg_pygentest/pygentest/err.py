from typing import Callable

from pygentype.err import CommonUserException
from pygentype.err import UserException


class SomeUserException(CommonUserException):
    @staticmethod
    def create(text_generator_fn: Callable[[], str]) -> UserException:
        return SomeUserException(text_generator_fn())
