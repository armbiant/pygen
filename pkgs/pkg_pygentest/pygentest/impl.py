from unittest import TestCase
from unittest import TestSuite


class TestCaseImpl(TestCase):
    def __str__(self) -> str:
        return "{}.{}".format(
            self.__class__.__module__,
            self.__class__.__qualname__
        )


class TestSuiteImpl(TestSuite):
    pass
