import os

from typing import Sequence

import pygentest.project.pypackage.utils

from pygenpath.abc import PathResolver
from pygentest.impl import TestCaseImpl
from pygentest.impl import TestSuiteImpl
from pygentest.project.pypackage.impl import ProjectPyPackageTestCaseImpl


class ProjectPkgsDirTestCaseImpl(TestCaseImpl):
    _path_resolver: PathResolver
    _pkgs: Sequence[str]

    def __init__(
        self,
        path_resolver: PathResolver,
        pkgs: Sequence[str]
    ) -> None:
        super().__init__()

        self._path_resolver = path_resolver
        self._pkgs = pkgs

    def runTest(self) -> None:
        _, dirnames, filenames = next(os.walk(self._path_resolver.pwd()))

        with (self.subTest("filenames")):
            self.assertEqual(
                first=[],
                second=filenames
            )

        with (self.subTest("dirnames")):
            self.assertEqual(
                first=list(
                    map(self._map_pkg_to_dirname, self._pkgs)
                ),
                second=sorted(dirnames)
            )

    def _map_pkg_to_dirname(self, pkg: str) -> str:
        return "pkg_{}".format(pkg)


class ProjectTestsPkgsDirTestCaseImpl(TestCaseImpl):
    _path_resolver: PathResolver
    _pkgs: Sequence[str]

    def __init__(
        self,
        path_resolver: PathResolver,
        pkgs: Sequence[str]
    ) -> None:
        super().__init__()

        self._path_resolver = path_resolver
        self._pkgs = pkgs

    def runTest(self) -> None:
        package_dir = pygentest.project.pypackage.utils.load(
            path_resolver=self._path_resolver
        )

        with (self.subTest("filenames")):
            self.assertEqual(
                first=["__init__.py"],
                second=package_dir.filenames
            )

        with (self.subTest("dirnames")):
            self.assertEqual(
                first=package_dir.dirnames,
                second=self._pkgs
            )


class ProjectPkgsDirPyPackageTestSuiteImpl(TestSuiteImpl):
    def __init__(
        self,
        package: str,
        path_resolver: PathResolver,
        allowed_module_filenames: Sequence[str]
    ) -> None:
        super().__init__()

        package_path_resolver = path_resolver.cd(
            path="./{}/".format(package)
        )
        package_dir = pygentest.project.pypackage.utils.load(
            path_resolver=package_path_resolver
        )
        self.addTest(
            test=ProjectPyPackageTestCaseImpl(
                package=package,
                filenames=package_dir.filenames,
                required_filenames=["__init__.py", "py.typed"],
                optional_filenames=[
                    "impl.py",
                    "abc.py",
                    "mock.py",
                    "utils.py",
                    "err.py"
                ]
            )
        )
        for dirname in package_dir.dirnames:
            self._add_test(
                package="{}.{}".format(package, dirname),
                path_resolver=package_path_resolver.cd(
                    path="./{}/".format(dirname)
                )
            )

    def _add_test(self, package: str, path_resolver: PathResolver) -> None:
        package_dir = pygentest.project.pypackage.utils.load(path_resolver)

        self.addTest(
            test=ProjectPyPackageTestCaseImpl(
                package=package,
                filenames=package_dir.filenames,
                required_filenames=["__init__.py"],
                optional_filenames=[
                    "impl.py",
                    "abc.py",
                    "mock.py",
                    "utils.py",
                    "err.py"
                ]
            )
        )

        for dirname in package_dir.dirnames:
            self._add_test(
                package="{}.{}".format(package, dirname),
                path_resolver=path_resolver.cd(
                    path="./{}/".format(dirname)
                )
            )


class ProjectPkgsDirTestSuiteImpl(TestSuiteImpl):
    @staticmethod
    def _find_pkgs(pkgs_path_resolver: PathResolver) -> Sequence[str]:
        _, dirnames, _ = next(os.walk(pkgs_path_resolver.pwd()))

        pkg_dirnames = filter(
            lambda dirname: dirname.startswith("pkg_"),
            dirnames
        )

        pkgs = list(
            map(
                lambda dirname: dirname[4:],
                pkg_dirnames
            )
        )

        return sorted(pkgs)

    def __init__(self, workdir_path_resolver: PathResolver) -> None:
        super().__init__()

        pkgs_path_resolver = workdir_path_resolver.cd("./pkgs/")
        tests_pkgs_path_resolver = workdir_path_resolver.cd("./tests/pkgs/")

        pkgs = self._find_pkgs(pkgs_path_resolver)

        self.addTest(
            test=ProjectPkgsDirTestCaseImpl(
                path_resolver=pkgs_path_resolver,
                pkgs=pkgs
            )
        )

        self.addTest(
            test=ProjectTestsPkgsDirTestCaseImpl(
                path_resolver=tests_pkgs_path_resolver,
                pkgs=pkgs
            )
        )

        for pkg in pkgs:
            self.addTest(
                test=ProjectPkgsDirPyPackageTestSuiteImpl(
                    path_resolver=pkgs_path_resolver.cd(
                        path="./pkg_{}/".format(pkg)
                    ),
                    package=pkg,
                    allowed_module_filenames=[]
                )
            )
