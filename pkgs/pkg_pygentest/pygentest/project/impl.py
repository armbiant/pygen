from pygenpath.abc import PathResolver
from pygentest.impl import TestCaseImpl
from pygentest.impl import TestSuiteImpl


class ProjectIsortCfgTestCaseImpl(TestCaseImpl):
    _workdir_path_resolver: PathResolver

    def __init__(self, workdir_path_resolver: PathResolver) -> None:
        super().__init__()

        self._workdir_path_resolver = workdir_path_resolver

    def runTest(self) -> None:
        f = open(self._workdir_path_resolver.resolve("./.isort.cfg"))
        with (f):
            actual = f.read()

        self.assertEqual(
            first=(
                "[settings]\n"
                "force_single_line = true\n"
                "lines_between_types = 1\n"
                "multi_line_output = 7\n"
                "skip = \"\"\n"
            ),
            second=actual
        )


class ProjectPyreConfigurationTestCaseImpl(TestCaseImpl):
    _workdir_path_resolver: PathResolver

    def __init__(self, workdir_path_resolver: PathResolver) -> None:
        super().__init__()

        self._workdir_path_resolver = workdir_path_resolver

    def runTest(self) -> None:
        f = open(self._workdir_path_resolver.resolve("./.pyre_configuration"))
        with (f):
            actual = f.read()

        self.assertEqual(
            first=(
                "{\n"
                "  \"strict\": true,\n"
                "  \"source_directories\": [\n"
                "    \"./\"\n"
                "  ],\n"
                "  \"site_package_search_strategy\": \"pep561\",\n"
                "  \"search_path\": [\n"
                "    \"./pkgs/*/\",\n"
                "    \"./stubs/\"\n"
                "  ]\n"
                "}\n"
            ),
            second=actual
        )


class ProjectTestSuiteImpl(TestSuiteImpl):
    def __init__(self, workdir_path_resolver: PathResolver) -> None:
        super().__init__()

        self.addTest(
            test=ProjectIsortCfgTestCaseImpl(workdir_path_resolver)
        )
        self.addTest(
            test=ProjectPyreConfigurationTestCaseImpl(workdir_path_resolver)
        )
