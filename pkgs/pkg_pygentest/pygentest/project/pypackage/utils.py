import os

from pygenpath.abc import PathResolver
from pygentest.project.pypackage.abc import ProjectPyPackageDir


def load(path_resolver: PathResolver) -> ProjectPyPackageDir:
    _, dirnames, package_filenames = next(os.walk(path_resolver.pwd()))

    tmp_dirnames = ["__pycache__"]
    package_dirnames = list(
        filter(lambda dirname: (dirname not in tmp_dirnames), dirnames)
    )

    return ProjectPyPackageDir(
        filenames=sorted(package_filenames),
        dirnames=sorted(package_dirnames)
    )
