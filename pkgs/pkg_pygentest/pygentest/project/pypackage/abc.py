from typing import NamedTuple
from typing import Sequence


class ProjectPyPackageDir(NamedTuple):
    filenames: Sequence[str]
    dirnames: Sequence[str]
