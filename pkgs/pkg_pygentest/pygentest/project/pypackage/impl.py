from typing import Sequence

import pygentype.utils

from pygentest.impl import TestCaseImpl


class ProjectPyPackageTestCaseImpl(TestCaseImpl):
    _package: str
    _filenames: Sequence[str]
    _required_filenames: Sequence[str]
    _optional_filenames: Sequence[str]

    def __init__(
        self,
        package: str,
        filenames: Sequence[str],
        required_filenames: Sequence[str],
        optional_filenames: Sequence[str]
    ) -> None:
        super().__init__()

        self._package = package
        self._filenames = filenames
        self._required_filenames = required_filenames
        self._optional_filenames = optional_filenames

    def __str__(self) -> str:
        return "{} [{}]".format(
            super().__str__(),
            self._package
        )

    def runTest(self) -> None:
        testable_required_filenames, testable_optional_filenames = \
            pygentype.utils.split_into_bucket(
                items=self._filenames,
                fn=lambda filename: (filename in self._required_filenames)
            )

        with (self.subTest("required filenames")):
            self.assertEqual(
                first=self._required_filenames,
                second=testable_required_filenames
            )

        with (self.subTest("optional filenames")):
            for filename in testable_optional_filenames:
                self.assertIn(
                    member=filename,
                    container=self._optional_filenames
                )
