from abc import ABC
from typing import Mapping
from typing import Sequence

from pygentest.testcase.abc import TestCase
from pygentest.testcase.collection.abc import CollectionItemType


class ABCCollectionTestCaseImpl(TestCase, ABC):
    def assertCollectionOrdered(
        self,
        item_name: str,
        items: Sequence[CollectionItemType],
        params: Mapping[str, object] = {}
    ) -> None:
        ordered_items = sorted(items)
        for item_index in range(0, len(items)):
            actual = str(items[item_index])
            expected = str(ordered_items[item_index])
            sub_test_params = {
                **params,
                item_name: expected
            }
            with (self.subTest(**sub_test_params)):
                self.assertEqual(
                    first=expected,
                    second=actual
                )

    def assertCollectionUnique(
        self,
        item_name: str,
        items: Sequence[CollectionItemType],
        params: Mapping[str, object] = {}
    ) -> None:
        ordered_items = sorted(
            map(lambda item: str(item), items),
            reverse=True
        )
        while (len(ordered_items) > 0):
            item = ordered_items.pop()
            sub_test_params = {
                **params,
                item_name: item
            }
            with (self.subTest(**sub_test_params)):
                self.assertTrue(item not in ordered_items)

    def assertCollectionEqual(
        self,
        item_name: str,
        items: Sequence[CollectionItemType],
        required_items: Sequence[CollectionItemType] = [],
        optional_items: Sequence[CollectionItemType] = [],
        exclude_items: Sequence[CollectionItemType] = [],
        params: Mapping[str, object] = {}
    ) -> None:
        for item in required_items:
            sub_test_params = {
                **params,
                item_name: str(item),
                "state": "required"
            }
            with (self.subTest(**sub_test_params)):
                self.assertTrue(
                    expr=(item in items)
                )

        unexpected_items = set(items).difference(
            required_items,
            exclude_items
        )
        for item in unexpected_items:
            sub_test_params = {
                **params,
                item_name: str(item),
                "state": "unexpected"
            }
            with (self.subTest(**sub_test_params)):
                self.assertTrue(False)
