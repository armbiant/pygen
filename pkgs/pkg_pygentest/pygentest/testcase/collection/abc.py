from typing import Any
from typing import Protocol
from typing import TypeVar


class CollectionItem(Protocol):
    def __str__(self) -> str:
        raise NotImplementedError()

    def __lt__(self, __x: Any) -> bool:
        raise NotImplementedError()


CollectionItemType = TypeVar(
    name="CollectionItemType",
    bound=CollectionItem,
    covariant=True
)
