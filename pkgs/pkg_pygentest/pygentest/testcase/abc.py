from abc import ABC
from typing import ContextManager
from typing import Type


class TestCaseContext(ContextManager[object], ABC):
    exception: Exception


class TestCase:
    def assertTrue(
        self,
        expr: bool,
        msg: str = ...
    ) -> None:
        raise NotImplementedError()

    def assertFalse(
        self,
        expr: bool,
        msg: str = ...
    ) -> None:
        raise NotImplementedError()

    def assertEqual(
        self,
        first: object,
        second: object,
        msg: str = ...
    ) -> None:
        raise NotImplementedError()

    def subTest(
        self,
        msg: object = ...,
        **params: object
    ) -> ContextManager[None]:
        raise NotImplementedError()

    def assertRaises(
        self,
        expected_exception: Type[Exception],
        *args: object,
        **kwargs: object
    ) -> TestCaseContext:
        raise NotImplementedError()
