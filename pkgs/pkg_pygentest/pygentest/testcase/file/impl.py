import hashlib
import os

from abc import ABC
from typing import Set

from pygenpath.abc import PathResolver
from pygentest.testcase.abc import TestCase
from pygentype.abc import ReadonlySet


class ABCFileTestCaseImpl(TestCase, ABC):
    def filepathSet(
        self,
        workdir_path_resolver: PathResolver
    ) -> ReadonlySet[str]:
        workdir_dirpath = workdir_path_resolver.pwd()[:-1]

        filepath_set: Set[str] = set()
        for dirpath, _, filenames in os.walk(workdir_dirpath):
            path_resolver = workdir_path_resolver.cd("{}/".format(dirpath))
            for filename in filenames:
                filepath = path_resolver.resolve("./{}".format(filename))
                rel_filepath = ".{}".format(
                    filepath[len(workdir_dirpath):]
                )
                filepath_set.add(rel_filepath)

        return filepath_set

    def assertDirContentEqual(
        self,
        expected_path_resolver: PathResolver,
        actual_path_resolver: PathResolver
    ) -> None:
        expected_filepath_set = self.filepathSet(expected_path_resolver)
        actual_filepath_set = self.filepathSet(actual_path_resolver)

        unexpected_filepath_set = actual_filepath_set.difference(
            expected_filepath_set
        )
        for filepath in unexpected_filepath_set:
            unexpected_filepath_sub_test = self.subTest(
                filepath=filepath,
                state="unexpected"
            )
            with (unexpected_filepath_sub_test):
                self.assertTrue(False)

        absent_filepath_set = expected_filepath_set.difference(
            actual_filepath_set
        )
        for filepath in absent_filepath_set:
            absent_filepath_sub_test = self.subTest(
                filepath=filepath,
                state="absent"
            )
            with (absent_filepath_sub_test):
                self.assertTrue(False)

        equal_filepath_path_set = actual_filepath_set.intersection(
            expected_filepath_set
        )
        for filepath in equal_filepath_path_set:
            equal_filepath_sub_test = self.subTest(
                filepath=filepath,
                state="equal"
            )
            with (equal_filepath_sub_test):
                f = open(expected_path_resolver.resolve(filepath), "rb")
                with (f):
                    expected = hashlib.md5(f.read())
                f = open(actual_path_resolver.resolve(filepath), "rb")
                with (f):
                    actual = hashlib.md5(f.read())

                self.assertTrue(
                    expr=(expected.hexdigest() == actual.hexdigest())
                )
