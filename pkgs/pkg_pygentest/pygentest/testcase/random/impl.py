import random
import string

from typing import Callable
from typing import List
from typing import Sequence
from typing import Set
from typing import TypeVar

from pygentest.testcase.impl import TestCaseImpl
from pygentype.err import UnreachableStateException

T = TypeVar("T")


class RandomTestCaseImpl(TestCaseImpl):
    _random_values: Set[object]

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

        self._random_values = set()

    def randomValue(
        self,
        factory_fn: Callable[[], T]
    ) -> T:
        RETRIES_LIMIT = 10

        for _ in range(0, RETRIES_LIMIT):
            new_value = factory_fn()
            if (new_value not in self._random_values):
                self._random_values.add(new_value)

                return new_value

        raise UnreachableStateException()

    def randomText(self, size: int = 10) -> str:
        def factory() -> str:
            text = ""
            for _ in range(0, size):
                text += random.choice(string.ascii_lowercase)

            return text

        return self.randomValue(factory)

    def randomBool(self) -> bool:
        return self.randomValue(
            lambda: random.choice([True, False])
        )

    def randomFloat(self) -> float:
        return self.randomValue(
            lambda: random.uniform(0, 1)
        )

    def randomInt(self) -> int:
        return self.randomValue(
            lambda: random.randint(1, 0xffff)
        )

    def randomOrder(self, items: Sequence[T]) -> Sequence[T]:
        result: List[T] = []

        indexes: List[int] = list(range(0, len(items)))
        while (len(indexes) > 0):
            index = indexes.pop(random.randint(0, len(indexes) - 1))
            result.append(items[index])

        return result
