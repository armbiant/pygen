from typing import Generic

from pygenarg.abc import Arg
from pygenarg.abc import ArgContext
from pygenlogdev.logger.mock import LoggerMock
from pygenlogdev.mock import LogEntryMock
from pygentest.err import SomeUserException
from pygentest.testcase.random.impl import RandomTestCaseImpl


class ABCArgTestCaseImpl(RandomTestCaseImpl, Generic[ArgContext]):
    def assertDescribeEqual(
        self,
        description: str,
        value: str,
        arg: Arg[ArgContext]
    ) -> None:
        log = []

        arg.describe(
            logger=LoggerMock(log)
        )

        self.assertEqual(
            first=[
                LogEntryMock.info(
                    msg="{}: [{}]".format(description, value)
                )
            ],
            second=log
        )

    def assertWithWrapUserException(
        self,
        description: str,
        value: str,
        arg: Arg[ArgContext]
    ) -> None:
        user_err_sub_test = self.subTest(
            err="user"
        )
        with (user_err_sub_test):
            user_exception = SomeUserException.create(self.randomText)

            user_err_ctx = self.assertRaises(SomeUserException)
            with (user_err_ctx):
                with (arg):
                    raise user_exception

            self.assertEqual(
                first=user_exception.wrap(
                    title="{}: [{}]".format(description, value)
                ),
                second=user_err_ctx.exception
            )

        system_err_sub_test = self.subTest(
            err="system"
        )
        with (system_err_sub_test):
            system_exception = Exception()

            system_err_ctx = self.assertRaises(Exception)
            with (system_err_ctx):
                with (arg):
                    raise system_exception

            self.assertEqual(
                first=system_exception,
                second=system_err_ctx.exception
            )
