class YamlLoader:
    def load_from_file(self, path: str) -> object:
        raise NotImplementedError()
