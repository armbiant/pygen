import yaml

from pygentype.err import OSUserException
from pygentype.err import UserException
from pygenyaml.abc import YamlLoader
from yaml import MarkedYAMLError
from yaml.error import Mark


class YamlLoaderImpl(YamlLoader):
    @staticmethod
    def create() -> YamlLoader:
        return YamlLoaderImpl()

    def load_from_file(self, path: str) -> object:
        try:
            stream = open(
                file=path,
                mode="r"
            )
        except OSError as e:
            raise OSUserException.create(e)

        try:
            with (stream):
                return yaml.safe_load(stream)
        except MarkedYAMLError as e:
            if (e.problem_mark is not None):
                mark: Mark = e.problem_mark

                raise UserException(
                    msg="YAML syntax error [{}:{}:{}]: [{}]".format(
                        path,
                        mark.line + 1,
                        mark.column + 1,
                        e.problem
                    )
                )
            else:
                raise e
