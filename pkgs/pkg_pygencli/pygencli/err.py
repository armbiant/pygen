from pygentype.err import CommonUserException


class CliException(CommonUserException):
    pass
