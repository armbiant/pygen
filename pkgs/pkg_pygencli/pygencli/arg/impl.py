from typing import Mapping

from pygencli.arg.abc import CliArg
from pygencli.arg.abc import CliArgContext
from pygencli.err import CliException
from pygentype.err import UserException


class CliArgImpl(CliArg, CliArgContext):
    @staticmethod
    def create(metavar: str) -> CliArg:
        return CliArgImpl(metavar)

    _metavar: str

    def __init__(self, metavar: str) -> None:
        self._metavar = metavar

    def value(self, args: Mapping[str, str]) -> str:
        value = args.get(self._metavar)

        if (isinstance(value, str)):
            return value
        else:
            raise CliException("Undefined value")

    def metavar(self) -> str:
        return self._metavar

    def __enter__(self) -> CliArgContext:
        return self

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        if (isinstance(value, UserException)):
            raise value.wrap(
                title="Arg [{}]".format(
                    self._metavar
                )
            )
