from typing import Mapping


class CliArgContext:
    def value(self, args: Mapping[str, str]) -> str:
        raise NotImplementedError()


class CliArg:
    def metavar(self) -> str:
        raise NotImplementedError()

    def __enter__(self) -> CliArgContext:
        raise NotImplementedError()

    def __exit__(
        self,
        type: object,
        value: object,
        traceback: object
    ) -> None:
        raise NotImplementedError()
