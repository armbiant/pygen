from argparse import ArgumentParser
from typing import Mapping
from typing import Sequence
from typing import Set

from pygencli.cmd.abc import CliCmd
from pygencli.cmd.abc import CliRootCmd
from pygencli.cmd.abc import CliRootSimpleCmd
from pygencli.cmd.abc import CliSimpleCmd
from pygencli.err import CliException
from pygencli.parser.abc import CliParser
from pygentype.abc import ReadonlySet


class CliParserImpl(CliParser):
    @staticmethod
    def create(
        command: CliRootCmd
    ) -> CliParser:
        parser = ArgumentParser()
        flags: Set[str] = set()

        CliParserImpl._add_command(parser, flags, command)

        return CliParserImpl(
            parser=parser,
            flags=flags
        )

    @staticmethod
    def _add_command(
        parser: ArgumentParser,
        flags: Set[str],
        command: CliCmd
    ) -> None:
        for option in command.options():
            parser.add_argument(
                "--{}".format(option.name()),
                dest=option.arg().metavar()
            )
            flags.add(option.arg().metavar())

        if (isinstance(command, CliSimpleCmd)):
            pass
        elif (isinstance(command, CliRootSimpleCmd)):
            pass
        else:
            subparsers = parser.add_subparsers(
                dest=command.arg().metavar(),
                metavar=command.arg().metavar()
            )

            for command in command.commands():
                CliParserImpl._add_command(
                    parser=subparsers.add_parser(
                        command.name()
                    ),
                    flags=flags,
                    command=command
                )

    _parser: ArgumentParser
    _flags: ReadonlySet[str]

    def __init__(
        self,
        parser: ArgumentParser,
        flags: ReadonlySet[str]
    ) -> None:
        self._parser = parser
        self._flags = flags

    def parse(
        self,
        args: Sequence[str],
        envs: Mapping[str, str]
    ) -> Mapping[str, str]:
        parsed_args = vars(self._parser.parse_args(args))

        for key, value in parsed_args.items():
            if (key not in self._flags):
                continue
            elif (value is None):
                parsed_args[key] = envs.get(key)
            elif (key in envs):
                msg_tpl = " ".join([
                    "Environment variable [{}]",
                    "conflicts with corresponding command-line argument"
                ])

                raise CliException(
                    msg=msg_tpl.format(key)
                )

        return parsed_args
