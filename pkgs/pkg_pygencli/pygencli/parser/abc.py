from typing import Mapping
from typing import Sequence


class CliParser:
    def parse(
        self,
        args: Sequence[str],
        envs: Mapping[str, str]
    ) -> Mapping[str, str]:
        raise NotImplementedError()
