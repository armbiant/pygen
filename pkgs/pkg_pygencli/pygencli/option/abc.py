from pygencli.arg.abc import CliArg


class CliOption:
    def name(self) -> str:
        raise NotImplementedError()

    def arg(self) -> CliArg:
        raise NotImplementedError()
