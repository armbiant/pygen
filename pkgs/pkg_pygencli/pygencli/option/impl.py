from pygencli.arg.abc import CliArg
from pygencli.option.abc import CliOption


class CliOptionImpl(CliOption):
    @staticmethod
    def create(name: str, arg: CliArg) -> CliOption:
        return CliOptionImpl(name, arg)

    _name: str
    _arg: CliArg

    def __init__(self, name: str, arg: CliArg) -> None:
        self._name = name
        self._arg = arg

    def name(self) -> str:
        return self._name

    def arg(self) -> CliArg:
        return self._arg
