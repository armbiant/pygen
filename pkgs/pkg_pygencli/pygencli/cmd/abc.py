from __future__ import annotations

from typing import Iterable

from pygencli.arg.abc import CliArg
from pygencli.option.abc import CliOption


class CliSimpleCmd:
    def name(self) -> str:
        raise NotImplementedError()

    def options(self) -> Iterable[CliOption]:
        raise NotImplementedError()


class CliComplexCmd:
    def name(self) -> str:
        raise NotImplementedError()

    def arg(self) -> CliArg:
        raise NotImplementedError()

    def options(self) -> Iterable[CliOption]:
        raise NotImplementedError()

    def commands(self) -> Iterable[CliSimpleCmd | CliComplexCmd]:
        raise NotImplementedError()


class CliRootSimpleCmd:
    def options(self) -> Iterable[CliOption]:
        raise NotImplementedError()


class CliRootComplexCmd:
    def arg(self) -> CliArg:
        raise NotImplementedError()

    def options(self) -> Iterable[CliOption]:
        raise NotImplementedError()

    def commands(self) -> Iterable[CliSimpleCmd | CliComplexCmd]:
        raise NotImplementedError()


CliRootCmd = CliRootSimpleCmd | CliRootComplexCmd
CliCmd = CliSimpleCmd | CliComplexCmd | CliRootSimpleCmd | CliRootComplexCmd
