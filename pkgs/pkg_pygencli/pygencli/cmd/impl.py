from typing import Sequence

from pygencli.arg.abc import CliArg
from pygencli.cmd.abc import CliComplexCmd
from pygencli.cmd.abc import CliRootComplexCmd
from pygencli.cmd.abc import CliRootSimpleCmd
from pygencli.cmd.abc import CliSimpleCmd
from pygencli.option.abc import CliOption


class CliSimpleCmdImpl(CliSimpleCmd):
    @staticmethod
    def create(
        name: str,
        options: Sequence[CliOption]
    ) -> CliSimpleCmd:
        return CliSimpleCmdImpl(
            name=name,
            options=options
        )

    _name: str
    _options: Sequence[CliOption]

    def __init__(
        self,
        name: str,
        options: Sequence[CliOption]
    ) -> None:
        self._name = name
        self._options = options

    def name(self) -> str:
        return self._name

    def options(self) -> Sequence[CliOption]:
        return self._options


class CliComplexCmdImpl(CliComplexCmd):
    @staticmethod
    def create(
        name: str,
        arg: CliArg,
        options: Sequence[CliOption],
        commands: Sequence[CliSimpleCmd | CliComplexCmd]
    ) -> CliComplexCmd:
        return CliComplexCmdImpl(
            name=name,
            arg=arg,
            options=options,
            commands=commands
        )

    _name: str
    _arg: CliArg
    _options: Sequence[CliOption]
    _commands: Sequence[CliSimpleCmd | CliComplexCmd]

    def __init__(
        self,
        name: str,
        arg: CliArg,
        options: Sequence[CliOption],
        commands: Sequence[CliSimpleCmd | CliComplexCmd]
    ) -> None:
        self._name = name
        self._arg = arg
        self._options = options
        self._commands = commands

    def name(self) -> str:
        return self._name

    def arg(self) -> CliArg:
        return self._arg

    def options(self) -> Sequence[CliOption]:
        return self._options

    def commands(self) -> Sequence[CliSimpleCmd | CliComplexCmd]:
        return self._commands


class CliRootSimpleCmdImpl(CliRootSimpleCmd):
    @staticmethod
    def create(
        options: Sequence[CliOption]
    ) -> CliRootSimpleCmd:
        return CliRootSimpleCmdImpl(
            options=options
        )

    _options: Sequence[CliOption]

    def __init__(
        self,
        options: Sequence[CliOption]
    ) -> None:
        self._options = options

    def options(self) -> Sequence[CliOption]:
        return self._options


class CliRootComplexCmdImpl(CliRootComplexCmd):
    @staticmethod
    def create(
        arg: CliArg,
        options: Sequence[CliOption],
        commands: Sequence[CliSimpleCmd | CliComplexCmd]
    ) -> CliRootComplexCmd:
        return CliRootComplexCmdImpl(
            arg=arg,
            options=options,
            commands=commands
        )

    _arg: CliArg
    _options: Sequence[CliOption]
    _commands: Sequence[CliSimpleCmd | CliComplexCmd]

    def __init__(
        self,
        arg: CliArg,
        options: Sequence[CliOption],
        commands: Sequence[CliSimpleCmd | CliComplexCmd]
    ) -> None:
        self._arg = arg
        self._options = options
        self._commands = commands

    def arg(self) -> CliArg:
        return self._arg

    def options(self) -> Sequence[CliOption]:
        return self._options

    def commands(self) -> Sequence[CliSimpleCmd | CliComplexCmd]:
        return self._commands
