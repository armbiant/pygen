import os
import tempfile

from pygenpath.abc import PathResolver
from pygentype.err import UserException


class PathResolverImpl(PathResolver):
    @staticmethod
    def temp() -> PathResolver:
        return PathResolverImpl(
            workdir="{}/".format(tempfile.mkdtemp())
        )

    @staticmethod
    def create(workdir: str) -> PathResolver:
        path_resolver = PathResolverImpl(
            workdir="{}/".format(os.getcwd())
        )

        return path_resolver.cd(workdir)

    _workdir: str

    def __init__(self, workdir: str) -> None:
        self._workdir = workdir

    def resolve(self, path: str) -> str:
        filename = os.path.basename(path)

        self._validate_filename(filename)

        dirpath = path[:-len(filename)]

        if (dirpath == "/"):
            workdir = "/"
        else:
            workdir = self._validate_and_resolve_dirpath(dirpath)

        return workdir + filename

    def cd(self, path: str) -> PathResolver:
        filename = os.path.basename(path)
        dirpath = path

        if (filename != ""):
            raise UserException(
                msg="A directory path requires a trailing slash"
            )

        workdir = self._validate_and_resolve_dirpath(dirpath)

        return PathResolverImpl(workdir)

    def pwd(self) -> str:
        return self._workdir

    def _validate_filename(self, filename: str) -> None:
        invalid_filenames = ["", ".", ".."]

        if (filename in invalid_filenames):
            raise UserException(
                msg="A directory path instead of a file path"
            )

    def _validate_dirpath_prefix(self, path: str) -> None:
        valid_path_prefixes = ["/", "./", "../"]
        for path_prefix in valid_path_prefixes:
            if (path.startswith(path_prefix)):
                return

        raise UserException(
            msg="A path should start with {}".format(
                valid_path_prefixes
            )
        )

    def _validate_dirpath_normalization(self, path: str) -> None:
        if (path in ("/", "./")):
            return

        while (True):
            invalid_paths = ["././", "./../"]
            if (path in invalid_paths):
                break

            if (path.startswith("./")):
                actual = path[2:-1]
            else:
                actual = path[:-1]

            expected = os.path.normpath(path)
            if (actual != expected):
                break

            return

        raise UserException("A path should be normalized")

    def _validate_and_resolve_dirpath(self, path: str) -> str:
        self._validate_dirpath_prefix(path)
        self._validate_dirpath_normalization(path)

        return self._resolve_dirpath(path)

    def _resolve_dirpath(self, path: str) -> str:
        rel_path = os.path.join(self._workdir, path)
        abs_path = os.path.abspath(rel_path)

        if (abs_path == "/"):
            return "/"
        else:
            return "{}/".format(abs_path)
