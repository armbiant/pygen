from __future__ import annotations


class PathResolver:
    def resolve(self, path: str) -> str:
        raise NotImplementedError()

    def cd(self, path: str) -> PathResolver:
        raise NotImplementedError()

    def pwd(self) -> str:
        raise NotImplementedError()
