from typing import List

from pygenshell.client.abc import ShellClient
from pygenshell.std.stream.abc import ShellStdStream


class ShellClientMock(ShellClient):
    _calls: List[List[str]]

    def __init__(self) -> None:
        self._calls = []

    def call(
        self,
        args: List[str],
        stdout: ShellStdStream,
        stderr: ShellStdStream
    ) -> None:
        self._calls.append(args)

    def calls(self) -> List[List[str]]:
        return self._calls
