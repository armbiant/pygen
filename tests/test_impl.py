import os
import unittest

from typing import Sequence
from unittest import TestCase
from unittest import TestLoader
from unittest import TestSuite

from pygenpath.impl import PathResolverImpl
from pygentest.project.impl import ProjectTestSuiteImpl
from pygentest.project.pkgsdir.impl import ProjectPkgsDirTestSuiteImpl


def load_tests(
    loader: TestLoader,
    tests: Sequence[TestCase],
    pattern: str
) -> TestSuite:
    tests_path_resolver = PathResolverImpl.create(
        workdir="{}/".format(os.path.dirname(__file__))
    )
    workdir_path_resolver = tests_path_resolver.cd("../")

    suite = TestSuite()

    suite.addTest(ProjectTestSuiteImpl(workdir_path_resolver))
    suite.addTest(ProjectPkgsDirTestSuiteImpl(workdir_path_resolver))

    return suite


if (__name__ == "__main__"):
    unittest.main()
