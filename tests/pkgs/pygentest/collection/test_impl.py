from pygentest.testcase.collection.impl import ABCCollectionTestCaseImpl
from pygentest.testcase.random.impl import RandomTestCaseImpl
from pygentestdev.testcase.mock import TestCaseMock


class CollectionTestCaseMock(
    TestCaseMock,
    ABCCollectionTestCaseImpl
):
    ...


class TestCollectionTestCase(RandomTestCaseImpl):
    def test_assertCollectionOrdered(self) -> None:
        test_case = CollectionTestCaseMock()

        item_name = self.randomText()

        first_item = "a{}".format(self.randomText())
        second_item = "b{}".format(self.randomText())

        param_name = self.randomText()
        param_value = self.randomText()

        test_case.assertCollectionOrdered(
            item_name=item_name,
            items=[
                second_item,
                first_item
            ],
            params={
                param_name: param_value
            }
        )

        self.assertEqual(
            first=[
                test_case.exception(
                    params={
                        param_name: param_value,
                        item_name: first_item
                    },
                    msg=""
                ),
                test_case.exception(
                    params={
                        param_name: param_value,
                        item_name: second_item
                    },
                    msg=""
                )
            ],
            second=test_case.exceptions()
        )

    def test_assertCollectionUnique(self) -> None:
        test_case = CollectionTestCaseMock()

        item_name = self.randomText()

        bar_item = self.randomText()
        foo_item = self.randomText()

        param_name = self.randomText()
        param_value = self.randomText()

        test_case.assertCollectionUnique(
            item_name=item_name,
            items=[
                foo_item,
                bar_item,
                foo_item
            ],
            params={
                param_name: param_value
            }
        )

        self.assertEqual(
            first=[
                test_case.exception(
                    params={
                        param_name: param_value,
                        item_name: foo_item
                    },
                    msg=""
                )
            ],
            second=test_case.exceptions()
        )

    def test_assertCollectionEqual(self) -> None:
        test_case = CollectionTestCaseMock()

        item_name = self.randomText()

        required_item = self.randomText()
        unexpected_item = self.randomText()
        exclude_item = self.randomText()

        param_name = self.randomText()
        param_value = self.randomText()

        test_case.assertCollectionEqual(
            item_name=item_name,
            items=[
                unexpected_item,
                exclude_item
            ],
            required_items=[
                required_item
            ],
            optional_items=[
            ],
            exclude_items=[
                exclude_item
            ],
            params={
                param_name: param_value
            }
        )

        self.assertEqual(
            first=[
                test_case.exception(
                    params={
                        param_name: param_value,
                        item_name: required_item,
                        "state": "required"
                    },
                    msg=""
                ),
                test_case.exception(
                    params={
                        param_name: param_value,
                        item_name: unexpected_item,
                        "state": "unexpected"
                    },
                    msg=""
                )
            ],
            second=test_case.exceptions()
        )
