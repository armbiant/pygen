import os

from pygenpath.impl import PathResolverImpl
from pygentest.testcase.abc import TestCase
from pygentest.testcase.file.impl import ABCFileTestCaseImpl
from pygentestdev.testcase.mock import TestCaseMock


class FileTestCaseMock(
    TestCaseMock,
    ABCFileTestCaseImpl
):
    ...


class FileTestCaseTestImpl(TestCase):
    def test_assertDirContentEqual(self) -> None:
        test_path_resolver = PathResolverImpl.create(
            workdir="{}/".format(os.path.dirname(__file__))
        )

        sample_path_resolver = test_path_resolver.cd(
            path="../../../../samples/pygentest/file/"
        )
        expected_path_resolver = sample_path_resolver.cd("./expected/")
        actual_path_resolver = sample_path_resolver.cd("./actual/")

        case = FileTestCaseMock()

        case.assertDirContentEqual(
            expected_path_resolver=expected_path_resolver,
            actual_path_resolver=actual_path_resolver
        )

        self.assertEqual(
            first=[
                case.exception(
                    params={
                        "filepath": "./ertcxxpxez/cjaoimefov",
                        "state": "unexpected"
                    },
                    msg=""
                ),
                case.exception(
                    params={
                        "filepath": "./ucmxpqboyj/cjaoimefov",
                        "state": "absent"
                    },
                    msg=""
                ),
                case.exception(
                    params={
                        "filepath": "./gtjtufuqqz",
                        "state": "equal"
                    },
                    msg=""
                )
            ],
            second=case.exceptions()
        )
