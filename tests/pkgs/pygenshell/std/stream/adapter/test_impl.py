
from pygenlogdev.logger.mock import LoggerMock
from pygenlogdev.mock import LogEntryMock
from pygenshell.std.stream.adapter.impl import ShellStdStreamAdapterImpl
from pygentest.testcase.random.impl import RandomTestCaseImpl


class ShellStdStreamAdapterTest(RandomTestCaseImpl):
    def test_write(self) -> None:
        foo = self.randomText()
        bar = "{}"
        baz = "%s"

        log = []
        logger = LoggerMock(log)
        stream = ShellStdStreamAdapterImpl.create_unclassified_logger(logger)

        stream.write(foo)
        stream.write(bar)
        stream.write(baz)

        self.assertEqual(
            first=[
                LogEntryMock.unclassified(foo),
                LogEntryMock.unclassified(bar),
                LogEntryMock.unclassified(baz)
            ],
            second=log
        )
