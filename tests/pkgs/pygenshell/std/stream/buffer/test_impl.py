from pygenshell.std.stream.buffer.impl import ShellStdStreamBufferImpl
from pygentest.testcase.random.impl import RandomTestCaseImpl


class ShellStdStreamBufferTest(RandomTestCaseImpl):
    def test_write(self) -> None:
        foo = self.randomText()
        bar = self.randomText()
        baz = self.randomText()

        stream = ShellStdStreamBufferImpl()

        stream.write(foo)
        stream.write(bar)
        stream.write(baz)

        self.assertEqual(
            first="{}{}{}".format(foo, bar, baz),
            second=stream.text()
        )
