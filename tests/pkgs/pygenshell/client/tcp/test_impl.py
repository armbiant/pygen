from pygenshell.client.tcp.impl import ShellTcpClientImpl
from pygenshell.std.stream.buffer.impl import ShellStdStreamBufferImpl
from pygenshelltest.server.tcp.mock import ShellTcpServerMock
from pygentest.testcase.random.impl import RandomTestCaseImpl
from pygentype.err import ExpectedException


class ShellTcpClientTest(RandomTestCaseImpl):
    def test_call_success(self) -> None:
        err_foo = self.randomText()
        out_foo = self.randomText()
        err_bar = self.randomText()
        out_bar = self.randomText()

        response = (
            "se:{}\n".format(err_foo),
            "so:{}\n".format(out_foo),
            "se:{}\n".format(err_bar),
            "so:{}\n".format(out_bar),
            "rc:{}\n".format(0)
        )

        stdout = ShellStdStreamBufferImpl()
        stderr = ShellStdStreamBufferImpl()
        server = ShellTcpServerMock("".join(response))
        with (server):
            shell_client = ShellTcpClientImpl(
                address=server.address()
            )
            shell_client.call(
                args=[""],
                stdout=stdout,
                stderr=stderr
            )

        self.assertEqual(
            first="{}{}".format(out_foo, out_bar),
            second=stdout.text()
        )
        self.assertEqual(
            first="{}{}".format(err_foo, err_bar),
            second=stderr.text()
        )

    def test_call_failed(self) -> None:
        rc = self.randomInt()
        server = ShellTcpServerMock(
            response="rc:{}\n".format(rc)
        )
        with (server):
            shell = ShellTcpClientImpl(
                address=server.address()
            )
            err_ctx = self.assertRaises(ExpectedException)
            with (err_ctx):
                shell.call(
                    args=[""],
                    stdout=ShellStdStreamBufferImpl(),
                    stderr=ShellStdStreamBufferImpl()
                )

        self.assertEqual(
            first=ExpectedException(
                msg="Return code [{}]".format(rc)
            ),
            second=err_ctx.exception
        )
