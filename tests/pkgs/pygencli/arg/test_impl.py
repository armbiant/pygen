from pygencli.arg.impl import CliArgImpl
from pygencli.err import CliException
from pygentest.err import SomeUserException
from pygentest.testcase.random.impl import RandomTestCaseImpl


class TestCliArg(RandomTestCaseImpl):
    def test_value(self) -> None:
        metavar = self.randomText()
        value = self.randomText()

        arg = CliArgImpl(metavar)

        self.assertEqual(
            first=value,
            second=arg.value(
                args={
                    metavar: value
                }
            )
        )

    def test_value_undefined_value(self) -> None:
        arg = CliArgImpl("")

        err_ctx = self.assertRaises(CliException)
        with (err_ctx):
            arg.value({})

        self.assertEqual(
            first=CliException("Undefined value"),
            second=err_ctx.exception
        )

    def test_with_wrap_user_exception(self) -> None:
        metavar = self.randomText()
        exception = SomeUserException.create(self.randomText)

        arg = CliArgImpl.create(metavar)

        err_ctx = self.assertRaises(SomeUserException)
        with (err_ctx):
            with (arg):
                raise exception

        self.assertEqual(
            first=exception.wrap(
                title="Arg [{}]".format(metavar)
            ),
            second=err_ctx.exception
        )
