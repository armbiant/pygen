import pygenrandom.utils

from pygencli.cmd.impl import CliComplexCmdImpl
from pygencli.cmd.impl import CliRootComplexCmdImpl
from pygencli.cmd.impl import CliRootSimpleCmdImpl
from pygencli.cmd.impl import CliSimpleCmdImpl
from pygencli.err import CliException
from pygencli.parser.impl import CliParserImpl
from pygenclitest.arg.mock import CliArgMock
from pygenclitest.option.mock import CliOptionMock
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygentest.testcase.impl import TestCaseImpl


class TestParser(TestCaseImpl):
    def test_parse_arg_and_env_conflict(self) -> None:
        option = CliOptionMock.create(pygenrandom.utils.randtext)

        parser = CliParserImpl.create(
            command=CliRootSimpleCmdImpl(
                options=[
                    option
                ]
            )
        )

        err_ctx = self.assertRaises(CliException)
        with (err_ctx):
            parser.parse(
                args=[
                    "--{}".format(option.name()),
                    ""
                ],
                envs={
                    option.arg().metavar(): ""
                }
            )

        err_msg_tpl = " ".join([
            "Environment variable [{}]",
            "conflicts with corresponding command-line argument"
        ])

        self.assertEqual(
            first=CliException(
                msg=err_msg_tpl.format(option.arg().metavar())
            ),
            second=err_ctx.exception
        )

    def test_parse(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        # ./foo bar baz --help
        # options:
        #   baz-foo - ...
        #   baz-bar - ...

        baz_command_name = text_generator.randtext()
        baz_foo_option = CliOptionMock.create(text_generator.randtext)
        baz_bar_option = CliOptionMock.create(text_generator.randtext)
        baz_cmd = CliSimpleCmdImpl.create(
            name=baz_command_name,
            options=[
                baz_foo_option,
                baz_bar_option
            ]
        )

        # ./foo bar --help
        # options:
        #   bar-foo - ...
        #   bar-bar - ...
        # commands:
        #   baz - ...

        bar_command_name = text_generator.randtext()
        bar_command_arg = CliArgMock.create(text_generator.randtext)
        bar_foo_option = CliOptionMock.create(text_generator.randtext)
        bar_bar_option = CliOptionMock.create(text_generator.randtext)
        bar_cmd = CliComplexCmdImpl.create(
            name=bar_command_name,
            arg=bar_command_arg,
            options=[
                bar_foo_option,
                bar_bar_option
            ],
            commands=[
                baz_cmd
            ]
        )

        # ./foo --help
        # options:
        #   foo-foo - ...
        #   foo-bar - ...
        # commands:
        #   bar - ...

        foo_command_arg = CliArgMock.create(text_generator.randtext)
        foo_foo_option = CliOptionMock.create(text_generator.randtext)
        foo_bar_option = CliOptionMock.create(text_generator.randtext)
        foo_cmd = CliRootComplexCmdImpl.create(
            arg=foo_command_arg,
            options=[
                foo_foo_option,
                foo_bar_option
            ],
            commands=[
                bar_cmd
            ]
        )

        parser = CliParserImpl.create(
            command=foo_cmd
        )

        foo_foo_option_value = text_generator.randtext()
        bar_foo_option_value = text_generator.randtext()
        baz_foo_option_value = text_generator.randtext()

        foo_bar_option_value = text_generator.randtext()
        bar_bar_option_value = text_generator.randtext()
        baz_bar_option_value = text_generator.randtext()

        actual_args = parser.parse(
            args=[
                "--{}".format(foo_foo_option.name()),
                foo_foo_option_value,
                bar_command_name,
                "--{}".format(bar_foo_option.name()),
                bar_foo_option_value,
                baz_command_name,
                "--{}".format(baz_foo_option.name()),
                baz_foo_option_value
            ],
            envs={
                foo_bar_option.arg().metavar(): foo_bar_option_value,
                bar_bar_option.arg().metavar(): bar_bar_option_value,
                baz_bar_option.arg().metavar(): baz_bar_option_value
            }
        )

        expected_args = {
            foo_command_arg.metavar(): bar_command_name,
            bar_command_arg.metavar(): baz_command_name,

            foo_foo_option.arg().metavar(): foo_foo_option_value,
            bar_foo_option.arg().metavar(): bar_foo_option_value,
            baz_foo_option.arg().metavar(): baz_foo_option_value,

            foo_bar_option.arg().metavar(): foo_bar_option_value,
            bar_bar_option.arg().metavar(): bar_bar_option_value,
            baz_bar_option.arg().metavar(): baz_bar_option_value
        }

        self.assertEqual(
            first=expected_args,
            second=actual_args
        )
