import os

from unittest.case import TestCase

from pygenpath.impl import PathResolverImpl
from pygentype.err import OSUserException
from pygentype.err import UserException
from pygenyaml.impl import YamlLoaderImpl


class YamlLoaderTestCaseImpl(TestCase):
    def assertInvalidYaml(
        self,
        path: str,
        line: int,
        column: int,
        err_msg: str
    ) -> None:
        test_path_resolver = PathResolverImpl.create(
            workdir="{}/".format(os.path.dirname(__file__))
        )

        sample_path_resolver = test_path_resolver.cd(
            path="../../../samples/pygenyaml/"
        )

        full_path = sample_path_resolver.resolve(path)

        err_ctx = self.assertRaises(UserException)
        with (err_ctx):
            YamlLoaderImpl.create().load_from_file(full_path)

        self.assertEqual(
            first=UserException(
                msg="YAML syntax error [{}:{}:{}]: [{}]".format(
                    full_path,
                    line,
                    column,
                    err_msg
                )
            ),
            second=err_ctx.exception
        )


class TestYamlLoader(YamlLoaderTestCaseImpl):
    def test_load_file_not_found(self) -> None:
        err_ctx = self.assertRaises(OSUserException)
        with (err_ctx):
            YamlLoaderImpl.create().load_from_file("")

        self.assertEqual(
            first=OSUserException("File not found"),
            second=err_ctx.exception
        )

    def test_load_file_invalid_yaml(self) -> None:
        self.assertInvalidYaml(
            path="./invalid.yaml",
            line=2,
            column=1,
            err_msg="expected the node content, but found ']'"
        )
