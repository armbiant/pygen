from __future__ import annotations

from typing import Callable
from typing import NamedTuple
from typing import Sequence

from pygentest.testcase.random.impl import RandomTestCaseImpl
from pygentree.err import TreeUnexpectedNodesException
from pygentree.listnode.impl import TreeListNodeImpl
from pygentree.objectnode.impl import TreeObjectNodeImpl


class TreeListNodeTestCaseImpl(RandomTestCaseImpl):
    ...


class TestTreeListNode(TreeListNodeTestCaseImpl):
    def test_detach_node(self) -> None:
        class _Case(NamedTuple):
            tree_node: object
            expected_node_fn: Callable[[int], object]
            detach_node_fn: Callable[[TreeListNodeImpl], object]

        text_node_name = self.randomText()
        text_node_value = self.randomText()

        bool_node_name = self.randomText()
        bool_node_value = self.randomBool()

        float_node_name = self.randomText()
        float_node_value = self.randomFloat()

        int_node_name = self.randomText()
        int_node_value = self.randomInt()

        object_node_value = {
            text_node_name: text_node_value,
            bool_node_name: bool_node_value,
            float_node_name: float_node_value,
            int_node_name: int_node_value
        }

        cases: Sequence[_Case] = self.randomOrder(
            items=[
                _Case(
                    tree_node=object_node_value,
                    expected_node_fn=lambda index: TreeObjectNodeImpl.create(
                        tree=object_node_value,
                        path="$[{}]".format(index)
                    ),
                    detach_node_fn=TreeListNodeImpl.detach_object_node
                )
            ]
        )

        list_node = TreeListNodeImpl(
            path="$",
            tree=list(
                map(
                    lambda case: case.tree_node,
                    cases
                )
            )
        )

        for index in range(0, len(cases)):
            case = cases[index]

            sub_test = self.subTest(
                fn=case.detach_node_fn.__name__
            )
            with (sub_test):
                self.assertEqual(
                    first=case.expected_node_fn(index),
                    second=list_node.detach_object_node()
                )

        self.assertTrue(list_node.empty())


class TestTreeListNodeWith(RandomTestCaseImpl):
    def test_with_unexpected_nodes(self) -> None:
        path = self.randomText()

        for total_node_count in range(1, 3):
            for detached_node_count in range(0, total_node_count):
                sub_test = self.subTest(
                    total_node_count=total_node_count,
                    detached_node_count=detached_node_count
                )
                with (sub_test):
                    list_node = TreeListNodeImpl(
                        path=path,
                        tree=[{} for _ in range(0, total_node_count)]
                    )
                    err_ctx = self.assertRaises(TreeUnexpectedNodesException)
                    with (err_ctx):
                        with (list_node as list_node_ctx):
                            for _ in range(0, detached_node_count):
                                list_node_ctx.detach_object_node()

                    self.assertEqual(
                        first=TreeUnexpectedNodesException(
                            msg="Unexpected node(s) [{}[{}:{}]]".format(
                                path,
                                detached_node_count,
                                total_node_count - 1
                            )
                        ),
                        second=err_ctx.exception
                    )
