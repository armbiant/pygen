from typing import Callable
from typing import Iterable
from typing import Mapping
from typing import Sequence

from pygentest.testcase.random.impl import RandomTestCaseImpl
from pygentree.err import TreeInvalidNodeTypeException
from pygentree.err import TreeNodeNotFoundException
from pygentree.err import TreeUnexpectedNodesException
from pygentree.listnode.impl import TreeListNodeImpl
from pygentree.objectnode.impl import TreeObjectNodeImpl


class TreeObjectNodeCreateTestCaseImpl(RandomTestCaseImpl):
    def assertCreateRaisesInvalidNodeTypeException(
        self,
        tree: object
    ) -> None:
        sub_test = self.subTest(
            tree=tree
        )
        with (sub_test):
            err_ctx = self.assertRaises(TreeInvalidNodeTypeException)
            with (err_ctx):
                TreeObjectNodeImpl.create(
                    tree=tree
                )

            err_msg_tpl = (
                "Node [$]: Invalid node type. [{}] instead of [dict]"
            )

            self.assertEqual(
                first=TreeInvalidNodeTypeException(
                    msg=err_msg_tpl.format(
                        type(tree).__name__
                    )
                ),
                second=err_ctx.exception
            )

    def assertCreateRaisesInvalidNodeNameTypeException(
        self,
        name: object
    ) -> None:
        sub_test = self.subTest(
            name=name
        )
        with (sub_test):
            err_ctx = self.assertRaises(TreeInvalidNodeTypeException)
            with (err_ctx):
                TreeObjectNodeImpl.create(
                    tree={
                        name: None
                    }
                )

            err_msg_tpl = (
                "Invalid node name type. [{}] instead of str"
            )

            self.assertEqual(
                first=TreeInvalidNodeTypeException(
                    msg=err_msg_tpl.format(
                        type(name).__name__
                    )
                ),
                second=err_ctx.exception
            )


class TestTreeObjectNodeCreate(TreeObjectNodeCreateTestCaseImpl):
    def test_create_invalid_tree_type(self) -> None:
        self.assertCreateRaisesInvalidNodeTypeException(False)
        self.assertCreateRaisesInvalidNodeTypeException(0.0)
        self.assertCreateRaisesInvalidNodeTypeException(0)
        self.assertCreateRaisesInvalidNodeTypeException([])
        self.assertCreateRaisesInvalidNodeTypeException(set[object]())
        self.assertCreateRaisesInvalidNodeTypeException("")
        self.assertCreateRaisesInvalidNodeTypeException(object())

    def test_create_invalid_node_name_type(self) -> None:
        self.assertCreateRaisesInvalidNodeNameTypeException(False)
        self.assertCreateRaisesInvalidNodeNameTypeException(0.0)
        self.assertCreateRaisesInvalidNodeNameTypeException(0)


class TreeObjectNodeDetachNodeTestCaseImpl(RandomTestCaseImpl):
    def assertDetachNodeValueEqual(
        self,
        detach_node_fn: Callable[[TreeObjectNodeImpl, str], object],
        tree: Mapping[str, object],
        name: str,
        value: object
    ) -> None:
        sub_test = self.subTest(
            fn=detach_node_fn.__name__
        )
        with (sub_test):
            object_node = TreeObjectNodeImpl(
                path="$",
                tree=tree
            )

            self.assertEqual(
                first=value,
                second=detach_node_fn(object_node, name)
            )

    def assertDetachNodeRaisesNotFoundException(
        self,
        detach_node_fn: Callable[[TreeObjectNodeImpl, str], object]
    ) -> None:
        sub_test = self.subTest(
            fn=detach_node_fn.__name__
        )
        with (sub_test):
            path = self.randomText()
            name = self.randomText()

            object_node = TreeObjectNodeImpl(path, {})

            err_ctx = self.assertRaises(TreeNodeNotFoundException)
            with (err_ctx):
                detach_node_fn(object_node, name)

            self.assertEqual(
                first=TreeNodeNotFoundException(
                    msg="Node [{}.{}] not found".format(path, name)
                ),
                second=err_ctx.exception
            )

    def assertDetachNodeRaisesInvalidTypeException(
        self,
        detach_node_fn: Callable[[TreeObjectNodeImpl, str], object],
        values: Iterable[object]
    ) -> None:
        for value in values:
            sub_test = self.subTest(
                fn=detach_node_fn.__name__,
                value=value
            )
            with (sub_test):
                path = self.randomText()
                name = self.randomText()

                object_node = TreeObjectNodeImpl(
                    path=path,
                    tree={
                        name: value
                    }
                )

                err_ctx = self.assertRaises(TreeInvalidNodeTypeException)
                with (err_ctx):
                    detach_node_fn(object_node, name)


class TestTreeObjectNodeDetachNode(TreeObjectNodeDetachNodeTestCaseImpl):
    def test_detach_node(self) -> None:
        text_node_name = self.randomText()
        text_node_value = self.randomText()

        bool_node_name = self.randomText()
        bool_node_value = self.randomBool()

        float_node_name = self.randomText()
        float_node_value = self.randomFloat()

        int_node_name = self.randomText()
        int_node_value = self.randomInt()

        object_node_name = self.randomText()
        object_node_value = {
            text_node_name: text_node_value,
            bool_node_name: bool_node_value,
            float_node_name: float_node_value,
            int_node_name: int_node_value
        }

        list_node_name = self.randomText()
        list_node_value = [
            text_node_value,
            bool_node_value,
            float_node_value,
            int_node_value,
            object_node_value
        ]

        self.assertDetachNodeValueEqual(
            detach_node_fn=TreeObjectNodeImpl.detach_text_node,
            tree={
                text_node_name: text_node_value
            },
            name=text_node_name,
            value=text_node_value
        )

        self.assertDetachNodeValueEqual(
            detach_node_fn=TreeObjectNodeImpl.detach_bool_node,
            tree={
                bool_node_name: bool_node_value
            },
            name=bool_node_name,
            value=bool_node_value
        )

        self.assertDetachNodeValueEqual(
            detach_node_fn=TreeObjectNodeImpl.detach_float_node,
            tree={
                float_node_name: float_node_value
            },
            name=float_node_name,
            value=float_node_value
        )

        self.assertDetachNodeValueEqual(
            detach_node_fn=TreeObjectNodeImpl.detach_int_node,
            tree={
                int_node_name: int_node_value
            },
            name=int_node_name,
            value=int_node_value
        )

        self.assertDetachNodeValueEqual(
            detach_node_fn=TreeObjectNodeImpl.detach_object_node,
            tree={
                object_node_name: dict(object_node_value)
            },
            name=object_node_name,
            value=TreeObjectNodeImpl(
                path="$.{}".format(object_node_name),
                tree=object_node_value
            )
        )

        self.assertDetachNodeValueEqual(
            detach_node_fn=TreeObjectNodeImpl.detach_list_node,
            tree={
                list_node_name: list(list_node_value)
            },
            name=list_node_name,
            value=TreeListNodeImpl(
                path="$.{}".format(list_node_name),
                tree=list_node_value
            )
        )

    def test_detach_node_not_found(self) -> None:
        self.assertDetachNodeRaisesNotFoundException(
            detach_node_fn=TreeObjectNodeImpl.detach_text_node
        )
        self.assertDetachNodeRaisesNotFoundException(
            detach_node_fn=TreeObjectNodeImpl.detach_bool_node
        )
        self.assertDetachNodeRaisesNotFoundException(
            detach_node_fn=TreeObjectNodeImpl.detach_float_node
        )
        self.assertDetachNodeRaisesNotFoundException(
            detach_node_fn=TreeObjectNodeImpl.detach_int_node
        )
        self.assertDetachNodeRaisesNotFoundException(
            detach_node_fn=TreeObjectNodeImpl.detach_object_node
        )
        self.assertDetachNodeRaisesNotFoundException(
            detach_node_fn=TreeObjectNodeImpl.detach_list_node
        )

    def test_detach_node_invalid_type(self) -> None:
        text_node_value: object = ""
        bool_node_value: object = False
        float_node_value: object = 0.0
        int_node_value: object = 0
        object_node_value: object = {}
        list_node_value: object = {}

        values: Sequence[object] = [
            text_node_value,
            bool_node_value,
            float_node_value,
            int_node_value,
            object_node_value,
            list_node_value
        ]

        self.assertDetachNodeRaisesInvalidTypeException(
            detach_node_fn=TreeObjectNodeImpl.detach_text_node,
            values=filter(lambda v: v != text_node_value, values)
        )
        self.assertDetachNodeRaisesInvalidTypeException(
            detach_node_fn=TreeObjectNodeImpl.detach_bool_node,
            values=filter(lambda v: v != bool_node_value, values)
        )
        self.assertDetachNodeRaisesInvalidTypeException(
            detach_node_fn=TreeObjectNodeImpl.detach_float_node,
            values=filter(lambda v: v != float_node_value, values)
        )
        self.assertDetachNodeRaisesInvalidTypeException(
            detach_node_fn=TreeObjectNodeImpl.detach_int_node,
            values=filter(lambda v: v != int_node_value, values)
        )
        self.assertDetachNodeRaisesInvalidTypeException(
            detach_node_fn=TreeObjectNodeImpl.detach_object_node,
            values=filter(lambda v: v != object_node_value, values)
        )
        self.assertDetachNodeRaisesInvalidTypeException(
            detach_node_fn=TreeObjectNodeImpl.detach_list_node,
            values=filter(lambda v: v != list_node_value, values)
        )


class TestTreeObjectNodeWith(RandomTestCaseImpl):
    def test_with_unexpected_nodes(self) -> None:
        path = self.randomText()
        foo_node_name = self.randomText()
        bar_node_name = self.randomText()
        baz_node_name = self.randomText()

        object_node = TreeObjectNodeImpl(
            path=path,
            tree={
                foo_node_name: None,
                bar_node_name: None,
                baz_node_name: None
            }
        )

        err_ctx = self.assertRaises(TreeUnexpectedNodesException)
        with (err_ctx):
            with (object_node):
                ...

        self.assertEqual(
            first=TreeUnexpectedNodesException(
                msg="Unexpected node(s) [{}{}]".format(
                    path,
                    sorted([foo_node_name, bar_node_name, baz_node_name])
                )
            ),
            second=err_ctx.exception
        )
