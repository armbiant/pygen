from pygenarg.address.abc import AddressArgContext
from pygenarg.address.impl import AddressArgImpl
from pygenargtest.testcase.impl import ABCArgTestCaseImpl
from pygentype.address.impl import AddressImpl


class TestAddressArg(ABCArgTestCaseImpl[AddressArgContext]):
    def test_value(self) -> None:
        description = self.randomText()
        value = AddressImpl.create(
            address="{}:{}".format(self.randomText(), self.randomInt())
        )

        arg = AddressArgImpl(description, value)

        self.assertEqual(
            first=value,
            second=arg.value()
        )

    def test_describe(self) -> None:
        description = self.randomText()
        value = AddressImpl.create(
            address="{}:{}".format(self.randomText(), self.randomInt())
        )

        arg = AddressArgImpl(description, value)

        self.assertDescribeEqual(
            description=description,
            value=str(value),
            arg=arg
        )

    def test_with_wrap_user_exception(self) -> None:
        description = self.randomText()
        value = AddressImpl.create(
            address="{}:{}".format(self.randomText(), self.randomInt())
        )

        arg = AddressArgImpl(description, value)

        self.assertWithWrapUserException(
            description=description,
            value=str(value),
            arg=arg
        )
