from typing import Callable

from pygenarg.err import ArgEmptyValueException
from pygenarg.text.abc import TextArgContext
from pygenarg.text.impl import TextArgImpl
from pygenargtest.testcase.impl import ABCArgTestCaseImpl


class ABCTextArgTestCaseImpl(ABCArgTestCaseImpl[TextArgContext]):
    def assertRaisesExceptionOnEmptyValue(
        self,
        description: str,
        fn: Callable[[], object]
    ) -> None:
        err_ctx = self.assertRaises(ArgEmptyValueException)
        with (err_ctx):
            fn()

        self.assertEqual(
            first=ArgEmptyValueException(
                msg="[{}]: empty value".format(description)
            ),
            second=err_ctx.exception
        )


class TestTextArg(ABCTextArgTestCaseImpl):
    def test_create_required(self) -> None:
        description = self.randomText()
        value = self.randomText()

        arg = TextArgImpl.create_required(description, value)

        self.assertDescribeEqual(
            description=description,
            value=value,
            arg=arg
        )

    def test_create_required_raises_exception_on_empty_value(self) -> None:
        description = self.randomText()

        self.assertRaisesExceptionOnEmptyValue(
            description=description,
            fn=lambda: TextArgImpl.create_required(description, "")
        )

    def test_create_required_env(self) -> None:
        env_name = self.randomText()
        env_value = self.randomText()

        arg = TextArgImpl.create_required_env(
            envs={
                env_name: env_value
            },
            env_name=env_name
        )

        self.assertDescribeEqual(
            description="Env[{}]".format(env_name),
            value=env_value,
            arg=arg
        )

    def test_create_required_env_raises_exception_on_empty_value(self) -> None:
        env_name = self.randomText()

        self.assertRaisesExceptionOnEmptyValue(
            description="Env[{}]".format(env_name),
            fn=lambda: TextArgImpl.create_required_env({}, env_name)
        )

    def test_value(self) -> None:
        value = self.randomText()

        arg = TextArgImpl("", value)

        self.assertEqual(
            first=value,
            second=arg.value()
        )

    def test_with_wrap_user_exception(self) -> None:
        description = self.randomText()
        value = self.randomText()

        arg = TextArgImpl(description, value)

        self.assertWithWrapUserException(
            description=description,
            value=value,
            arg=arg
        )
