from pygenarg.path.abc import PathResolverArgContext
from pygenarg.path.impl import PathResolverArgImpl
from pygenargtest.testcase.impl import ABCArgTestCaseImpl
from pygenpath.impl import PathResolverImpl


class TestPathResolverArg(ABCArgTestCaseImpl[PathResolverArgContext]):
    def test_value(self) -> None:
        description = self.randomText()
        value = PathResolverImpl.create(
            workdir="/{}/".format(self.randomText())
        )

        arg = PathResolverArgImpl(description, value)

        self.assertEqual(
            first=value,
            second=arg.value()
        )

    def test_describe(self) -> None:
        description = self.randomText()
        value = PathResolverImpl.create(
            workdir="/{}/".format(self.randomText())
        )

        arg = PathResolverArgImpl(description, value)

        self.assertDescribeEqual(
            description=description,
            value=value.pwd(),
            arg=arg
        )

    def test_with_wrap_user_exception(self) -> None:
        description = self.randomText()
        value = PathResolverImpl.create(
            workdir="/{}/".format(self.randomText())
        )

        arg = PathResolverArgImpl(description, value)

        self.assertWithWrapUserException(
            description=description,
            value=value.pwd(),
            arg=arg
        )
