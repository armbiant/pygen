from typing import Callable
from typing import Mapping
from typing import NamedTuple
from typing import Sequence

from pygenpath.impl import PathResolverImpl
from pygentest.testcase.random.impl import RandomTestCaseImpl
from pygentype.err import UserException


class TestPathResolver(RandomTestCaseImpl):
    class Case(NamedTuple):
        workdir_tpl: str
        dirpath_tpl: str
        expected_dirpath_tpl: str

    _cases: Sequence[Case]
    _args: Mapping[str, str]

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

        self._cases = [
            self.Case("/", "/", "/"),
            self.Case("/{foo}/", "/", "/"),
            self.Case("/{foo}/{bar}/", "/", "/"),
            self.Case("/{foo}/{bar}/", "./", "/{foo}/{bar}/"),
            self.Case("/{foo}/{bar}/", "../", "/{foo}/"),
            self.Case("/{foo}/{bar}/", "../../", "/"),
            self.Case("/{foo}/{bar}/", "../{baz}/", "/{foo}/{baz}/"),
            self.Case("/{foo}/{bar}/", "../../{baz}/", "/{baz}/")
        ]
        self._args = {
            "foo": self.randomText(),
            "bar": self.randomText(),
            "baz": self.randomText()
        }

    def test_resolve(self) -> None:
        filename = self.randomText()

        for case in self._cases:
            workdir = case.workdir_tpl.format(**self._args)
            dirpath = case.dirpath_tpl.format(**self._args)
            expected_dirpath = case.expected_dirpath_tpl.format(**self._args)

            path_resolver = PathResolverImpl.create(workdir)

            sub_test = self.subTest(
                workdir=case.workdir_tpl,
                path=case.dirpath_tpl
            )
            with (sub_test):
                self.assertEqual(
                    first="{}{}".format(expected_dirpath, filename),
                    second=path_resolver.resolve(
                        path="{}{}".format(dirpath, filename)
                    )
                )

    def test_cd(self) -> None:
        for case in self._cases:
            workdir = case.workdir_tpl.format(**self._args)
            dirpath = case.dirpath_tpl.format(**self._args)
            expected_dirpath = case.expected_dirpath_tpl.format(**self._args)

            path_resolver = PathResolverImpl.create(workdir)

            sub_test = self.subTest(
                workdir=case.workdir_tpl,
                path=case.dirpath_tpl
            )
            with (sub_test):
                self.assertEqual(
                    first=expected_dirpath,
                    second=path_resolver.cd(dirpath).pwd()
                )


class TestPathResolverResolveDirectoryInsteadOfFile(RandomTestCaseImpl):
    class Case(NamedTuple):
        path_tpl: str

    _cases: Sequence[Case]
    _args: Mapping[str, str]

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

        self._cases = [
            self.Case(""),
            self.Case("."),
            self.Case(".."),
            self.Case("./{foo}/")
        ]
        self._args = {
            "foo": self.randomText()
        }

    def test_resolve_directory_instead_of_file(self) -> None:
        path_resolver = PathResolverImpl.create("./")
        for case in self._cases:
            path = case.path_tpl.format(**self._args)

            sub_test = self.subTest(
                path=case.path_tpl
            )
            with (sub_test):
                err_ctx = self.assertRaises(UserException)
                with (err_ctx):
                    path_resolver.resolve(path)

                self.assertEqual(
                    first=UserException(
                        msg="A directory path instead of a file path"
                    ),
                    second=err_ctx.exception
                )


class ABCPathResolverDirectoryRequiresTrailingSlashTestCaseImpl(
    RandomTestCaseImpl
):
    class Case(NamedTuple):
        path_tpl: str

    _cases: Sequence[Case]
    _args: Mapping[str, str]

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

        self._cases = [
            self.Case("./."),
            self.Case("./.."),
            self.Case("./{foo}")
        ]
        self._args = {
            "foo": self.randomText()
        }

    def assertDirectoryRequiresTrailingSlash(
        self,
        fn: Callable[[str], object]
    ) -> None:
        for case in self._cases:
            path = case.path_tpl.format(**self._args)

            sub_test = self.subTest(
                path=case.path_tpl
            )
            with (sub_test):
                err_ctx = self.assertRaises(UserException)
                with (err_ctx):
                    fn(path)

                self.assertEqual(
                    first=UserException(
                        msg="A directory path requires a trailing slash"
                    ),
                    second=err_ctx.exception
                )


class TestPathResolverDirectoryRequiresTrailingSlash(
    ABCPathResolverDirectoryRequiresTrailingSlashTestCaseImpl
):
    def test_create_directory_requires_trailing_slash(self) -> None:
        self.assertDirectoryRequiresTrailingSlash(
            fn=PathResolverImpl.create
        )

    def test_cd_directory_requires_trailing_slash(self) -> None:
        self.assertDirectoryRequiresTrailingSlash(
            fn=PathResolverImpl.create("./").cd
        )


class ABCPathResolverInvalidPathNormalizationTestCaseImpl(RandomTestCaseImpl):
    class Case(NamedTuple):
        path_tpl: str

    _cases: Sequence[Case]
    _args: Mapping[str, str]

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

        self._cases = [
            self.Case("//"),
            self.Case("/./"),
            self.Case("/././"),
            self.Case("././"),
            self.Case("./././"),
            self.Case("./.././"),
            self.Case("./{foo}/../")
        ]
        self._args = {
            "foo": self.randomText()
        }

    def assertInvalidPathNormalization(
        self,
        fn: Callable[[str], object]
    ) -> None:
        for case in self._cases:
            path = case.path_tpl.format(**self._args)

            sub_test = self.subTest(
                path=case.path_tpl
            )
            with (sub_test):
                err_ctx = self.assertRaises(UserException)
                with (err_ctx):
                    fn(path)

                self.assertEqual(
                    first=UserException(
                        msg="A path should be normalized"
                    ),
                    second=err_ctx.exception
                )


class TestPathResolverInvalidPathNormalization(
    ABCPathResolverInvalidPathNormalizationTestCaseImpl
):
    def test_create_invalid_path_normalization(self) -> None:
        self.assertInvalidPathNormalization(
            fn=PathResolverImpl.create
        )

    def test_resolve_invalid_path_normalization(self) -> None:
        filename = self.randomText()
        path_resolver = PathResolverImpl.create("./")

        self.assertInvalidPathNormalization(
            fn=lambda dirpath: path_resolver.resolve(
                "{}{}".format(dirpath, filename)
            )
        )

    def test_cd_invalid_path_normalization(self) -> None:
        path_resolver = PathResolverImpl.create("./")

        self.assertInvalidPathNormalization(
            fn=path_resolver.cd
        )


class ABCPathResolverInvalidPathPrefixTestCaseImpl(RandomTestCaseImpl):
    class Case(NamedTuple):
        path_tpl: str

    _cases: Sequence[Case]
    _args: Mapping[str, str]

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

        self._cases = [
            self.Case(""),
            self.Case("{foo}/")
        ]
        self._args = {
            "foo": self.randomText()
        }

    def assertInvalidPathPrefix(
        self,
        fn: Callable[[str], object]
    ) -> None:
        for case in self._cases:
            path = case.path_tpl.format(**self._args)

            sub_test = self.subTest(
                path=case.path_tpl
            )
            with (sub_test):
                err_ctx = self.assertRaises(UserException)
                with (err_ctx):
                    fn(path)

                self.assertEqual(
                    first=UserException(
                        msg="A path should start with ['/', './', '../']"
                    ),
                    second=err_ctx.exception
                )


class TestPathResolverInvalidPathPrefix(
    ABCPathResolverInvalidPathPrefixTestCaseImpl
):
    def test_create_invalid_path_prefix(self) -> None:
        self.assertInvalidPathPrefix(
            fn=PathResolverImpl.create
        )

    def test_resolve_invalid_path_prefix(self) -> None:
        filename = self.randomText()
        path_resolver = PathResolverImpl.create("./")

        self.assertInvalidPathPrefix(
            fn=lambda dirpath: path_resolver.resolve(
                "{}{}".format(dirpath, filename)
            )
        )

    def test_cd_invalid_path_prefix(self) -> None:
        self.assertInvalidPathPrefix(
            fn=PathResolverImpl.create("./").cd
        )
