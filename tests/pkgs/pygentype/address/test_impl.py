from pygentest.testcase.random.impl import RandomTestCaseImpl
from pygentype.address.impl import AddressImpl
from pygentype.err import UserException


class TestAddress(RandomTestCaseImpl):
    def test_create(self) -> None:
        host = self.randomText()
        port = self.randomInt()

        address = AddressImpl.create(
            address="{}:{}".format(host, port)
        )

        host_method_sub_test = self.subTest(
            method="host"
        )
        with (host_method_sub_test):
            self.assertEqual(
                first=host,
                second=address.host
            )

        port_method_sub_test = self.subTest(
            method="port"
        )
        with (port_method_sub_test):
            self.assertEqual(
                first=port,
                second=address.port
            )

    def test_create_undefined_port(self) -> None:
        address = self.randomText()

        err_ctx = self.assertRaises(UserException)
        with (err_ctx):
            AddressImpl.create(address)

        self.assertEqual(
            first=UserException(
                msg="Invalid address"
            ),
            second=err_ctx.exception
        )

    def test_create_invalid_port(self) -> None:
        host = self.randomText()
        port = self.randomText()

        err_ctx = self.assertRaises(UserException)
        with (err_ctx):
            AddressImpl.create(
                address="{}:{}".format(host, port)
            )

        self.assertEqual(
            first=UserException(
                msg="Invalid port"
            ),
            second=err_ctx.exception
        )
